// ================================================================================================================== //
// Common site scripts
// ================================================================================================================== //

!function ($) {
	$(function(){

		var animTime = 'fast';

		$('.i-checks').iCheck({
			checkboxClass: 'icheckbox_square-green'
		});

		$('.chosen-select').chosen({disable_search_threshold: 10});

		$('body').on('click', '.add-email', function() {
			addField($(this), 'email');
		}).on('click', '.add-phone', function() {
			addField($(this), 'phone');
		}).on('click', '.add-sender_email', function() {
			addField($(this), 'sender_email');
		}).on('click', '.add-sender_phone', function() {
			addField($(this), 'sender_phone');
		}).on('click', '.add-recipient_email', function() {
			addField($(this), 'recipient_email');
		}).on('click', '.add-recipient_phone', function() {
			addField($(this), 'recipient_phone');
		}).on('click', '.add-object_email', function() {
			addField($(this), 'object_email');
		}).on('click', '.add-object_phone', function() {
			addField($(this), 'object_phone');
		}).on('blur', '[name^=email_]', function() {
			removeField($(this), 'email');
		}).on('blur', '[name^=phone_]', function() {
			removeField($(this), 'phone');
		}).on('blur', '[name^=sender_email_]', function() {
			removeField($(this), 'sender_email');
		}).on('blur', '[name^=sender_phone_]', function() {
			removeField($(this), 'sender_phone');
		}).on('blur', '[name^=recipient_email_]', function() {
			removeField($(this), 'recipient_email');
		}).on('blur', '[name^=recipient_phone_]', function() {
			removeField($(this), 'recipient_phone');
		}).on('blur', '[name^=object_email_]', function() {
			removeField($(this), 'object_email');
		}).on('blur', '[name^=object_phone_]', function() {
			removeField($(this), 'object_phone');
		}).on('click', 'input,textarea', function() {
			$(this).removeClass('error');
		}).on('ifToggled', '.state-checkbox-filter', function() {
			filterTable();
		}).on('change', '.chosen-select-filter', function() {
			filterTable();
		}).on('click', '#requests-list tr', function() {
			window.location = '/orders/edit/' + $(this).attr('request_id');
		}).on('change keyup input click', '[name=weight],[name=cubage],[name=capacity]', function() {
			var $this = $(this);
			var val = $this.val();
			var reg = /[^0-9$]/;
			if (val.match(reg))
			{
				$this.val(val.replace(reg, ''));
			}

			if ($this.attr('name') == 'cubage')
			{
				$('#cubagePoints').html($this.val() * $('[name=cubage_points]').val());
			}
		});

		function filterTable()
		{
			var checboxes	= $('.state-checkbox');
			var selectors	= $('.chosen-select');
			var table	= $('#filterTable');
			var states	= [];
			var select	= new Object();
			var url		= '/orders?';
			var name, val, checkId, checked;

			// Clear table and show loading image
			table.html('<div class="text-center"><img width="25px" src="/assets/img/preloader.gif"/></div>');

			// Filter sender|recipient|client_id
			$.each(selectors, function(i, v) {
				name	= $(v).attr('name');
				val	= $(v).val();
				switch (name)
				{
					case 'sender':
					case 'recipient':
					case 'client_id':
						select[name] = val;
						url += name + '=' + val + '&';
					break;
				}
			});

			if (select['client_id'] == undefined)
			{
				select['client_id'] = $('[name=client_id]').val();
				url += 'client_id' + '=' + $('[name=client_id]').val() + '&';
			}

			// Filter states
			url += 'states=';
			$.each(checboxes, function(i, v) {
				checkId = $(v).attr('check_id');
				if (checkId !== undefined)
				{
					checked = $(v).is(':checked');
					states.push(checked);
					url += (checked ? '1' : '0') + '_';
				}
			});
			url = url.slice(0, -1);

			history.pushState({}, null, url);

			$.post(
				'/ajax/filter_orders',
				{
					states: states,
					select: select
				},
				function(res) {
					res = $.parseJSON(res);
					if (res.status == 1 && res.html)
					{
						table.html(res.html);
					}
				}
			);
		}

		/**
		 * Add input field
		 * @param	object	el	object $(this)
		 * @param	string	field	email|phone|sender_email|sender_phone|recipient_email|recipient_phone|object_email|object_phone
		 */
		function addField(el, field)
		{
			var formGroup	= el.parents('.form-group');
			var newId	= parseInt(formGroup.find('.form-control').attr('def_id')) + 1;
			var input	= formGroup.find('input');
			var inputVal	= $.trim(input.val());
			var innerHtml	= '';

			if (inputVal === '')
			{
				input.addClass('error');
			}
			else
			{
				el.parent().empty();

				innerHtml = '<div class="form-group"><div class="col-sm-offset-2 col-sm-9">';

				switch (field)
				{
					case 'email':
					case 'sender_email':
					case 'recipient_email':
					case 'object_email':
						innerHtml += '<input type="text" name="' + field + '_' + newId + '" def_id="' + newId + '" class="form-control"/></div>'
							+ '<div class="col-sm-1"><button type="button" class="btn btn-sm btn-primary add-' + field + '" title="Добавить email">+</button>';
					break;

					case 'phone':
					case 'sender_phone':
					case 'recipient_phone':
					case 'object_phone':
						innerHtml += '<input type="text" name="' + field + '_' + newId + '" def_id="' + newId + '" class="form-control" data-mask="+9 (999) 999-99-99"/></div>'
							+ '<div class="col-sm-1"><button type="button" class="btn btn-sm btn-primary add-' + field + '" title="Добавить телефон">+</button>';
					break;
				}

				innerHtml += '</div></div>';

				formGroup.after(innerHtml);
			}
		}

		/**
		 * Remove input field
		 * @param	object	el	object $(this)
		 * @param	string	field	email|phone|sender_email|sender_phone|recipient_email|recipient_phone|object_email|object_phone
		 */
		function removeField(el, field)
		{
			var formGroup	= el.parents('.form-group');
			var prevId	= parseInt(formGroup.find('.form-control').attr('def_id')) - 1;
			var inputVal	= $.trim(el.val());
			var innerHtml	= '';

			if (prevId > 0 && (inputVal === '' || inputVal === '+_ (___) ___-__-__'))
			{
				formGroup.remove();
				switch (field)
				{
					case 'email':
					case 'sender_email':
					case 'recipient_email':
					case 'object_email':
						innerHtml = '<button type="button" class="btn btn-sm btn-primary add-' + field + '" title="Добавить email">+</button>';
					break;

					case 'phone':
					case 'sender_phone':
					case 'recipient_phone':
					case 'object_phone':
						innerHtml = '<button type="button" class="btn btn-sm btn-primary add-' + field + '" title="Добавить телефон">+</button>';
					break;
				}

				$('[name=' + field + '_' + prevId + ']').parent().siblings('div.col-sm-1').html(innerHtml);
			}
		}

		// Init all
		function init_all()
		{
			init_ajax_forms();

			if($('textarea').length > 0)
			{
				init_textarea();
			}
		}
		init_all();

		function init_textarea()
		{
			$('textarea').autosize();
		}

		// Ajax forms
		function init_ajax_forms()
		{
			$('.ajax-form').unbind().submit(function(){
				var self = this;
				$(self).find('input[type="submit"]').attr('disabled','disabled');

				$.post($(this).attr('action'), $(this).serialize(), function(data){
					var response = $.parseJSON(data);

					$(self).find('input[type="submit"]').removeAttr('disabled');
					$(self).find('.has-error').removeClass('has-error');
					$(self).find('.help-block').hide();

					if(response.errors)
					{
						for(var n in response.errors)
						{
							$(self).find('*[name="'+n+'"]').parents('.form-group').addClass('has-error');
							$(self).find('*[name="'+n+'"]').parents('.form-group').find('.help-block').show();
						}
					}

					if(response.status == 1)
					{
						$(self).find('.form-success').fadeIn('fast');
						setTimeout(function(){
							$(self).find('.form-success').fadeOut('fast');
						}, 2000);
					}

				});
				return false;
			});
		}

		// Orders
		$('#orders-list tr').click(function(){
			var order_id = $(this).attr('order_id');
			$('#order-num').text(order_id);
			$('#order-info-body').load('/ajax/order_info/'+order_id, function(){
				init_all();
			});
		});

		$('#showRequests').click(function(){
			window.location = '/orders?client_id=' + $(this).attr('page_id');
		});

		$('.add-sender').click(function() {
			showHideSenderRecipient($(this), 'sender');
		});

		$('.add-recipient').click(function() {
			showHideSenderRecipient($(this), 'recipient');
		});

		/**
		 * Show/hide sender/recipient form
		 * @param obj el $(this)
		 * @param string type sender|recipient
		 */
		function showHideSenderRecipient(el, type)
		{
			if (type === 'sender' || type === 'recipient')
			{
				var upperType = type.replace(/\b[a-z]/g, function(f) { return f.toUpperCase(); });
				if (el.hasClass('add-' + type + '-off'))
				{
					// Hide form
					$('#hidden' + upperType + 'sData').addClass('hidden', animTime);
					// Change button to +
					el.html('+').attr('title', 'Добавить нового').removeClass('add-' + type + '-off', animTime);
					// Show select
					el.parent().siblings('.btn-group').find('.chosen-container').show(animTime);
					// Show info under select
					$('#show' + upperType + 'sInfo').show(animTime);
					// Clear form fields
					$('#hidden' + upperType + 'sData input, textarea').val('');
				}
				else
				{
					// Show form
					$('#hidden' + upperType + 'sData').removeClass('hidden', animTime);
					// Change button to -
					el.html('&ndash;').attr('title', 'Очистить форму').addClass('add-' + type + '-off', animTime);
					// Hide select
					el.parent().siblings('.btn-group').find('.chosen-container').hide(animTime);
					// Hide info under select
					$('#show' + upperType + 'sInfo').hide(animTime);
				}
			}
		}

		$('#addRequest').click(function() {
			var form = $('#formAddRequest');
			$.post(
				form.prop('action'),
				{
					client_id	: $('[name=client_id]').val(),
					state		: $('[name=state]').val(),
					sender		: $('[name=sender]').val(),
					recipient	: $('[name=recipient]').val(),
					weight		: $('[name=weight]').val(),
					cubage		: $('[name=cubage]').val(),
					capacity	: $('[name=capacity]').val(),
					type		: $('[name=type]').val(),
					add_sender	: $('[name=sender_name]').is(':visible'),
					add_recipient	: $('[name=recipient_name]').is(':visible'),
					form_data	: form.serializeArray()
				},
				function(res) {
					res = $.parseJSON(res);
					if (res.status == 1)
					{
						window.location = '/orders';
					}
				}
			);
			return false;
		});

		$('#editRequest').click(function() {
			var form = $('#formEditRequest');
			$.post(
				form.prop('action'),
				{
					order_id	: $('[name=order_id]').val(),
					client_id	: $('[name=client_id]').val(),
					state		: $('[name=state]').val(),
					sender		: $('[name=sender]').val(),
					recipient	: $('[name=recipient]').val(),
					weight		: $('[name=weight]').val(),
					cubage		: $('[name=cubage]').val(),
					capacity	: $('[name=capacity]').val(),
					type		: $('[name=type]').val(),
					add_sender	: $('[name=sender_name]').is(':visible'),
					add_recipient	: $('[name=recipient_name]').is(':visible'),
					form_data	: form.serializeArray()
				},
				function(res) {
					res = $.parseJSON(res);
					if (res.status == 1)
					{
						window.location = '/orders';
					}
				}
			);
			return false;
		});

		$('#addClient').click(function() {
			var form = $('#formAddClient');
			$.post(
				form.prop('action'),
				{
					name		: $('[name=name]').val(),
					manager_id	: $('[name=manager_id]').val(),
					form_data	: form.serializeArray()
				},
				function(res) {
					res = $.parseJSON(res);
					if (res.status == 1)
					{
						window.location = '/clients';
					}
					else if (res.errors)
					{
						for (var n in res.errors)
						{
							form.find('input[name="'+n+'"]').addClass('error').focus();
						}
						if (res.errors_add)
						{
							for (var n in res.errors_add)
							{
								form.find('input[name="'+n+'"]').addClass('error').focus();
							}
						}
					}
				}
			);
			return false;
		});

		$('#editClient').click(function() {
			var form = $('#formEditClient');
			$.post(
				form.prop('action'),
				{
					name		: $('[name=name]').val(),
					manager_id	: $('[name=manager_id]').val(),
					client_id	: $('[name=client_id]').val(),
					form_data	: form.serializeArray()
				},
				function(res) {
					res = $.parseJSON(res);
					if (res.status == 1)
					{
						window.location = '/clients';
					}
					else if (res.errors)
					{
						for (var n in res.errors)
						{
							form.find('input[name="'+n+'"]').addClass('error').focus();
						}
						if (res.errors_add)
						{
							for (var n in res.errors_add)
							{
								form.find('input[name="'+n+'"]').addClass('error').focus();
							}
						}
					}
				}
			);
			return false;
		});

		$('#addObject').click(function() {
			var form = $('#formAddObject');
			$.post(
				form.prop('action'),
				{
					name		: $('[name=name]').val(),
					client_id	: $('[name=client_id]').val(),
					address		: $('[name=address]').val(),
					form_data	: form.serializeArray()
				},
				function(res) {
					res = $.parseJSON(res);
					if (res.status == 1)
					{
						window.location = '/objects';
					}
					else if (res.errors)
					{
						for (var n in res.errors)
						{
							form.find('input[name="'+n+'"]').addClass('error').focus();
						}
						if (res.errors_add)
						{
							for (var n in res.errors_add)
							{
								form.find('input[name="'+n+'"]').addClass('error').focus();
							}
						}
					}
				}
			);
			return false;
		});

		$('#editObject').click(function() {
			var form = $('#formEditObject');
			$.post(
				form.prop('action'),
				{
					id		: $('[name=id]').val(),
					name		: $('[name=name]').val(),
					client_id	: $('[name=client_id]').val(),
					address		: $('[name=address]').val(),
					form_data	: form.serializeArray()
				},
				function(res) {
					res = $.parseJSON(res);
					if (res.status == 1)
					{
						window.location = '/objects';
					}
					else if (res.errors)
					{
						for (var n in res.errors)
						{
							form.find('input[name="'+n+'"]').addClass('error').focus();
						}
						if (res.errors_add)
						{
							for (var n in res.errors_add)
							{
								form.find('input[name="'+n+'"]').addClass('error').focus();
							}
						}
					}
				}
			);
			return false;
		});

		$('#editClientProfile').click(function() {
			var form = $('#formEditClientProfile');
			$.post(
				form.prop('action'),
				{
					name		: $('[name=name]').val(),
					manager_id	: $('[name=manager_id]').val(),
					client_id	: $('[name=client_id]').val(),
					notify_email	: $('[name=notify_email]').is(':checked'),
					notify_sms	: $('[name=notify_sms]').is(':checked'),
					form_data	: form.serializeArray()
				},
				function(res) {
					res = $.parseJSON(res);
					if (res.errors)
					{
						for (var n in res.errors)
						{
							form.find('input[name="'+n+'"]').addClass('error').focus();
						}
						if (res.errors_add)
						{
							for (var n in res.errors_add)
							{
								form.find('input[name="'+n+'"]').addClass('error').focus();
							}
						}
					}
				}
			);
			return false;
		});

	});
}(window.jQuery)