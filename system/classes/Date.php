<?php defined('SYSPATH') or die('No direct script access.');

class Date extends Kohana_Date {

	public static function format($date,$format='U',$offset=NULL)
	{
		$translation = array
		(
			"am" => "дп",
			"pm" => "пп",
			"AM" => "ДП",
			"PM" => "ПП",
			"Monday" => "Понедельник",
			"Mon" => "Пн",
			"Tuesday" => "Вторник",
			"Tue" => "Вт",
			"Wednesday" => "Среда",
			"Wed" => "Ср",
			"Thursday" => "Четверг",
			"Thu" => "Чт",
			"Friday" => "Пятница",
			"Fri" => "Пт",
			"Saturday" => "Суббота",
			"Sat" => "Сб",
			"Sunday" => "Воскресенье",
			"Sun" => "Вс",
			"January" => "января",
			"Jan" => "Янв",
			"February" => "февраля",
			"Feb" => "Фев",
			"March" => "марта",
			"Mar" => "Мар",
			"April" => "апреля",
			"Apr" => "Апр",
			"May" => "мая",
			"May" => "мая",
			"June" => "июня",
			"Jun" => "июн",
			"July" => "июля",
			"Jul" => "июл",
			"August" => "августа",
			"Aug" => "Авг",
			"September" => "сентября",
			"Sep" => "Сен",
			"October" => "октября",
			"Oct" => "Окт",
			"November" => "ноября",
			"Nov" => "Ноя",
			"December" => "декабря",
			"Dec" => "Дек",
			"st" => "ое",
			"nd" => "ое",
			"rd" => "е",
			"th" => "ое",
		);

		if (empty($date) || $date == '0000-00-00 00:00:00' || $date == '0000-00-00') return NULL;
		try
		{
			// Timestamp
			if(ctype_digit($date)) $date = date('Y-m-d H:i:s',$date);
			// Parse date
			$d = new DateTime($date);
			// Add offset
			if ($offset) $d->modify($offset);
			$formated = '';
			foreach(str_split($format) as $f)
			{
				$part = $d->format($f);
				// Translate
				$formated.= ctype_digit($part) ? $part : __($part);
			}

			return strtr($formated, $translation);
			//return $formated;
		}
		catch (Exception $e) {}
		return NULL;
	}
}