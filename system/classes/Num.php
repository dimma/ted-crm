<?php defined('SYSPATH') OR die('No direct script access.');

class Num extends Kohana_Num {

	public static function format($number, $places, $monetary = FALSE)
	{
		$info = localeconv();

		if ($monetary)
		{
			$decimal = $info['mon_decimal_point'];
		}
		else
		{
			$decimal = $info['decimal_point'];
		}

		$thousands = ' ';
		$decimal = ',';

		return number_format($number, $places, $decimal, $thousands);
	}
}
