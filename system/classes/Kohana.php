<?php defined('SYSPATH') OR die('No direct script access.');

class Kohana extends Kohana_Core {
	public static function ar($array)
	{
		echo '<pre>';
		print_r($array);
		echo '</pre>';
	}

	//	Сумма прописью
	public static function digit2string($digit)
	{
		$tmp_num = (integer) $digit;	// текущий
		$string	= "";
		$i = 1;				// счетчик триад
		$th = array("");

		// откусываем по три цифры с конца и обрабатываем функцией
		while(strlen($tmp_num) > 0)
		{
			$tri = "";
			if(strlen($tmp_num) > 3)
			{
				$dig = substr($tmp_num, strlen($tmp_num)-3);
				$tmp_num = substr($tmp_num, 0, -3);
			} else {
				$dig = $tmp_num;
				$tmp_num = "";
			}

			if($i == 1) $th = " ";
			elseif ($i == 2) $th = " тысяч ";
			elseif ($i == 3) $th = " миллионов ";
			elseif ($i == 4) $th = " миллиардов ";
			elseif ($i == 5) $th = " биллионов ";
			elseif ($i == 6) $th = " триллионов ";

			// вызываем функцию "сумма прописью" для нашей триады и присоединяем название разряда
			$string = (Kohana::StrDigit($dig) !== "") ? Kohana::StrDigit($dig) . $th . $string : "";
			$i++;
		}

		// а теперь заменим неправильные падежи - их не так много, поэтому обойдемся массивом замен
		$tr_arr = array(
			"один тысяч"		=> "одна тысяча",
			"два тысяч" 		=> "две тысячи",
			"три тысяч" 		=> "три тысячи",
			"четыре тысяч"		=> "четыре тысячи",
			"один миллионов"	=> "один миллион",
			"два миллионов"		=> "два миллиона",
			"три миллионов"		=> "три миллиона",
			"четыре миллионов"	=> "четыре миллиона",
			"один миллиардов"	=> "один миллиард",
			"два миллиардов"	=> "два миллиарда",
			"три миллиардов"	=> "три миллиарда",
			"четыре миллиардов"	=> "четыре миллиарда"
		);

		// заменяем падежи
		$string = strtr($string, $tr_arr);

		return($string);
	}

	private static function StrDigit($digit)
	{
		$string = "";

		// определяем массивы единиц, десятков и сотен
		$ed		= array("","один","два","три","четыре","пять","шесть","семь","восемь","девять","десять","одиннадцать","двенадцать","тринадцать","четырнадцать","пятнадцать","шестнадцать","семнадцать","восемнадцать","девятнадцать","двадцать");
		$des	= array("","десять","двадцать","тридцать","сорок","пятьдесят","шестьдесят","семьдесят","восемьдесят","девяносто");
		$sot	= array("","сто","двести","триста","четыреста","пятьсот","шестьсот","семьсот","восемьсот","девятьсот");

		$digit = (int) $digit;
		if ($digit > 0 && $digit <= 20)
		{
			$string = $ed[$digit];
		}
		elseif ($digit > 20 && $digit < 100)
		{
			$tmp1	= substr($digit, 0, 1);
			$tmp2	= substr($digit, 1, 1);
			$string	= $des[$tmp1] ." ". $ed[$tmp2];
		}
		else if ($digit > 99 && $digit < 1000)
		{
			$tmp1 = substr($digit, 0, 1);
			if (substr($digit, 1, 2) > 20)
			{
				$tmp2	= substr($digit, 1, 1);
				$tmp3	= substr($digit, 2, 1);
				$string	= $sot[$tmp1] ." ". $des[$tmp2] ." ". $ed[$tmp3];
			}
			else $string = $sot[$tmp1] ." ". Kohana::StrDigit(substr($digit, 1, 2));
		}

		return $string;
	}
}
