<?=View::factory('auth/_header', array('title' => $title, 'site_name' => $site_name))?>

<?=Form::open('/login', array('role' => 'form', 'class' => 'm-t', 'id' => 'auth-form'))?>
	<div class="form-group">
		<input type="text" name="username" class="form-control" placeholder="Логин" required=""/>
	</div>
	<div class="form-group">
		<input type="password" name="password" class="form-control" placeholder="Пароль" required=""/>
	</div>
	<button type="submit" class="btn btn-primary block full-width m-b">Войти</button>
	<div class="margin-cell"><a href="/register">Регистрация</a></div>
	<div class="margin-cell"><a href="http://tedtrans.com">Вернуться на tedtrans.com</a></div>
<?=Form::close()?>

<?=View::factory('auth/_footer')?>