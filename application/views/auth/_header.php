<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?=$title?> | <?=$site_name?></title>
		<link href="/assets/css/bootstrap.min.css" rel="stylesheet">
		<link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
		<link href="/assets/css/animate.css" rel="stylesheet">
		<link href="/assets/css/style.css" rel="stylesheet">
		<style type="text/css">
			.logo-wrap { text-align: center; margin-top: 100px; }
			.loginscreen { margin-top: -150px !important; }
		</style>
	</head>
	<body class="gray-bg">
		<div class="logo-wrap"><h1 class="logo-name"><?=$site_name;?></h1></div>
		<div class="middle-box text-center loginscreen animated fadeInDown">
			<div>
				<h3><?=$title?></h3>