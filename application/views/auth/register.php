<?=View::factory('auth/_header', array('title' => $title, 'site_name' => $site_name))?>

<?=Form::open('/register', array('role' => 'form', 'class' => 'm-t', 'id' => 'auth-form'))?>
	<div class="form-group">
		<input type="text" name="username" class="form-control" placeholder="Логин" required=""/>
	</div>
	<div class="form-group">
		<input type="password" name="password" class="form-control" placeholder="Пароль" required=""/>
	</div>
	<div class="form-group">
		<input type="text" name="name" class="form-control" placeholder="Ваше имя или название компании" required=""/>
	</div>
	<div class="form-group">
		<input type="email" name="email" class="form-control" placeholder="Email" required=""/>
	</div>
	<div class="form-group">
		<input type="tel" name="phone" class="form-control" placeholder="Телефон" required=""/>
	</div>
	<button type="submit" class="btn btn-primary block full-width m-b">Отправить</button>
	<div class="margin-cell"><a href="/login">Войти с существующего аккаунта</a></div>
	<div class="margin-cell"><a href="http://tedtrans.com">Вернуться на tedtrans.com</a></div>
<?=Form::close()?>

<?=View::factory('auth/_footer')?>