				<p class="m-t"><small>&copy; 2014, SiteExpo</small></p>
			</div>
		</div>
		<!-- Mainly scripts -->
		<script src="/assets/js/jquery-1.10.2.js"></script>
		<script src="/assets/js/bootstrap.min.js"></script>

		<script type="text/javascript">
			$(document).ready(function(){
				$('#auth-form input[name="username"]').focus();
			});
		</script>
	</body>
</html>
