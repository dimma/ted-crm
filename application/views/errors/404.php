<!DOCTYPE html>
<html lang="en">
	<head>
		<title>404</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php foreach ($styles as $file => $type) { echo HTML::style($file, array('media' => $type)), "\n"; } ?>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->

		<!-- include pace script for automatic web page progress bar  -->
		<script>paceOptions = { elements: true };</script>
		<script src="/assets/js/pace.min.js"></script>
	</head>
	<body>

		<div class='not_found'>
			<div class='empty_head'>
				<a href='/'><img src='/assets/images/logo_w.png'/></a>
				<p class='phone'>+7 (495) 781-57-45</p>
			</div>
			<div class='cont_empt'>
				<h1>404</h1>
				<p class='f_row'>Такой страницы не существует</p>
				<p>Начните с <a href='/'>главной</a></p>
			</div>
		</div>

		<!-- Javascripts ================================================== -->
		<?php foreach ($scripts as $file) { echo HTML::script($file), "\n"; } ?>
	</body>
</html>
