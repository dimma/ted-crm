<div id="filterTable">
	<?php if (!empty($total)) : ?>
		<div>Всего: <?=$total?> </div>
		<div class="solid">
			<table class="table table-hover data-table" id="sorting-table">
				<thead>
					<tr>
						<th class="thin text-right">&#8470;</th>
						<th>Отправитель</th>
						<th>Получатель</th>
						<th>Клиент</th>
						<th class="status-cell">Статус</th>
						<th>Дата</th>
					</tr>
				</thead>
				<tbody class="notes-list" id="requests-list">
					<?php foreach ($requests as $request) : ?>
						<tr request_id="<?=$request['id']?>">
							<td class="text-right id-cell"><strong class="request-id"><?=$request['id']?></strong></td>
							<td>
								<?php foreach ($stores as $s) : ?>
									<?php if ($s->id == $request['sender']) : ?>
										<?=$s->name?>
									<?php endif; ?>
								<?php endforeach; ?>
							</td>
							<td>
								<?php foreach ($stores as $s) : ?>
									<?php if ($s->id == $request['recipient']) : ?>
										<?=$s->name?>
									<?php endif; ?>
								<?php endforeach; ?>
							</td>
							<td><?=$request['client_name']?></td>
							<td><?=$request['state_title']?></td>
							<td data-sort="<?=Date::format($request['date'], 'd.m.Y, H:i')?>">
								<?php if (date('Y-m-d', strtotime($request['date'])) == date('Y-m-d')) : ?>
									Сегодня,&nbsp;<?=Date::format($request['date'], 'H:i')?>
								<?php elseif (date_diff(date_create(date('Y-m-d')), date_create(Date::format($request['date'], 'Y-m-d')))->format('%a') == 1) : ?>
									Вчера,&nbsp;<?=Date::format($request['date'], 'H:i')?>
								<?php else : ?>
									<?=Date::format($request['date'], 'd F, H:i')?>
								<?php endif; ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<div class="paging"><?=$pagination?></div>
	<?php else : ?>
		<div class="panel colourable">
			<div class="panel-body">
				<p>Список заявок пуст</p>
			</div>
		</div>
	<?php endif; ?>
</div>