<div class="wrapper wrapper-content product-info">
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox float-e-margins">
				<div class="ibox-content">
					<?php if (!empty($clients) || !empty($client)) : ?>
						<?=Form::open('/ajax/add_request', array('class' => 'form-horizontal ajax-form', 'id' => 'formAddRequest'))?>
						<?=Form::hidden('cubage_points', $cubage_points)?>
							<div class="form-group">
								<label class="col-sm-2 control-label">Клиент</label>
								<?php if ($is_admin) : ?>
									<div class="col-sm-9 btn-group">
										<select name="client_id" class="chosen-select">
											<?php foreach ($clients as $c) : ?>
												<option value="<?=$c->id?>"><?=$c->name?></option>
											<?php endforeach; ?>
										</select>
									</div>
								<?php else : ?>
									<?=Form::hidden('client_id', $client->id)?>
									<div class="col-sm-9 margin-cell"><?=$client->name?></div>
								<?php endif; ?>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label class="col-sm-2 control-label">Отправитель</label>
										<div class="col-sm-9 btn-group">
											<select name="sender" class="chosen-select">
												<option value="0">Выберите отправителя</option>
												<?php if (!empty($stores)) : ?>
													<?php foreach ($stores as $s) : ?>
														<option value="<?=$s->id?>"><?=$s->name?></option>
													<?php endforeach; ?>
												<?php endif; ?>
											</select>
										</div>
										<div class="col-sm-1">
											<button class="btn btn-sm btn-primary add-sender" title="Добавить нового" type="button">+</button>
										</div>
									</div>
									<div id="showSendersInfo"><!--
										<?php foreach ($stores as $s) : ?>
											<option value="<?=$s->id?>"><?=$s->name?></option>
										<?php endforeach; ?>-->
									</div>
									<div class="hidden" id="hiddenSendersData">
										<div class="form-group">
											<label class="col-sm-2 control-label">Наименование</label>
											<div class="col-sm-9">
												<input type="text" name="sender_name" class="form-control"/>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Адрес</label>
											<div class="col-sm-9 btn-group">
												<textarea name="sender_address" class="form-control no-resize"></textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Email</label>
											<div class="col-sm-9">
												<input type="text" name="sender_email_1" def_id="1" class="form-control"/>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-sm btn-primary add-sender_email" title="Добавить email" type="button">+</button>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Телефон</label>
											<div class="col-sm-9">
												<input type="text" name="sender_phone_1" def_id="1" class="form-control" data-mask="+9 (999) 999-99-99"/>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-sm btn-primary add-sender_phone" title="Добавить телефон" type="button">+</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label class="col-sm-2 control-label">Получатель</label>
										<div class="col-sm-9 btn-group">
											<select name="recipient" class="chosen-select">
												<option value="0">Выберите получателя</option>
												<?php if (!empty($stores)) : ?>
													<?php foreach ($stores as $s) : ?>
														<option value="<?=$s->id?>"><?=$s->name?></option>
													<?php endforeach; ?>
												<?php endif; ?>
											</select>
										</div>
										<div class="col-sm-1">
											<button class="btn btn-sm btn-primary add-recipient" title="Добавить нового" type="button">+</button>
										</div>
									</div>
									<div id="showRecipientsInfo"><!--
										<?php foreach ($stores as $s) : ?>
											<option value="<?=$s->id?>"><?=$s->name?></option>
										<?php endforeach; ?>-->
									</div>
									<div class="hidden" id="hiddenRecipientsData">
										<div class="form-group">
											<label class="col-sm-2 control-label">Наименование</label>
											<div class="col-sm-9">
												<input type="text" name="recipient_name" class="form-control"/>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Адрес</label>
											<div class="col-sm-9 btn-group">
												<textarea name="recipient_address" class="form-control no-resize"></textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Email</label>
											<div class="col-sm-9">
												<input type="text" name="recipient_email_1" def_id="1" class="form-control"/>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-sm btn-primary add-recipient_email" title="Добавить email" type="button">+</button>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Телефон</label>
											<div class="col-sm-9">
												<input type="text" name="recipient_phone_1" def_id="1" class="form-control" data-mask="+9 (999) 999-99-99"/>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-sm btn-primary add-recipient_phone" title="Добавить телефон" type="button">+</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="row">
								<div class="col-sm-6 b-r">
									<div class="form-group">
										<label class="col-sm-4 control-label">Вес груза</label>
										<div class="col-sm-6">
											<input type="text" name="weight" class="form-control"/>
										</div>
										<div class="col-sm-2">кг</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label">Кубатура</label>
										<div class="col-sm-6">
											<input type="text" name="cubage" class="form-control"/>
										</div>
										<div class="col-sm-2">м&#179;</div>
									</div>
									<div class="form-group">
										<div class="col-sm-4">&nbsp;</div>
										<div class="col-sm-8">
											за перевозку вы получите <span id="cubagePoints">0</span> points
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label">Количество мест</label>
										<div class="col-sm-6">
											<input type="text" name="capacity" class="form-control"/>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label">Характер груза</label>
										<div class="col-sm-6">
											<input type="text" name="type" class="form-control"/>
										</div>
									</div>
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-3 btn-group">
									<button class="btn btn-sm btn-primary" id="addRequest">Сохранить</button>
								</div>
							</div>
						<?=Form::close()?>
					<?php else : ?>
						Сначала <a href="/clients/add">добавьте клиентов</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>