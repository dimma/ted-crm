<div class="wrapper wrapper-content product-info">
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox float-e-margins">
				<div class="ibox-content">
					<?php if (!empty($order)) : ?>
						<?=Form::open('/ajax/add_request', array('class' => 'form-horizontal ajax-form', 'id' => 'formEditRequest'))?>
						<?=Form::hidden('client_id', $order->client_id)?>
						<?=Form::hidden('order_id', $order->id)?>
						<?=Form::hidden('cubage_points', $cubage_points)?>
							<div class="form-group">
								<label class="col-sm-2 control-label">Клиент</label>
								<div class="col-sm-9 request-client"><a href="/clients/edit/<?=$order->client_id?>"><?=$client->name?></a></div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label class="col-sm-2 control-label">Отправитель</label>
										<?php if ($can_edit) : ?>
											<div class="col-sm-9 btn-group">
												<select name="sender" class="chosen-select">
													<option value="0">Выберите отправителя</option>
													<?php if (!empty($stores)) : ?>
														<?php foreach ($stores as $s) : ?>
															<option value="<?=$s->id?>" <?php if ($s->id == $order->sender) : ?>selected="selected"<?php endif; ?>><?=$s->name?></option>
														<?php endforeach; ?>
													<?php endif; ?>
												</select>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-sm btn-primary add-sender" title="Добавить нового" type="button">+</button>
											</div>
										<?php else : ?>
											<div class="margin-cell">
												<?php if (!empty($stores)) : ?>
													<?php foreach ($stores as $s) : ?>
														<?php if ($s->id == $order->sender) : ?><?=$s->name?><?php endif; ?>
													<?php endforeach; ?>
												<?php endif; ?>
											</div>
										<?php endif; ?>
									</div>
									<div id="showSendersInfo"><!--
										<?php foreach ($stores as $s) : ?>
											<option value="<?=$s->id?>"><?=$s->name?></option>
										<?php endforeach; ?>-->
									</div>
									<div class="hidden" id="hiddenSendersData">
										<div class="form-group">
											<label class="col-sm-2 control-label">Наименование</label>
											<div class="col-sm-9">
												<input type="text" name="sender_name" class="form-control"/>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Адрес</label>
											<div class="col-sm-9 btn-group">
												<textarea name="sender_address" class="form-control no-resize"></textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Email</label>
											<div class="col-sm-9">
												<input type="text" name="sender_email_1" def_id="1" class="form-control"/>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-sm btn-primary add-sender_email" title="Добавить email" type="button">+</button>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Телефон</label>
											<div class="col-sm-9">
												<input type="text" name="sender_phone_1" def_id="1" class="form-control" data-mask="+9 (999) 999-99-99"/>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-sm btn-primary add-sender_phone" title="Добавить телефон" type="button">+</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label class="col-sm-2 control-label">Получатель</label>
										<?php if ($can_edit) : ?>
											<div class="col-sm-9 btn-group">
												<select name="recipient" class="chosen-select">
													<option value="0">Выберите получателя</option>
													<?php if (!empty($stores)) : ?>
														<?php foreach ($stores as $s) : ?>
															<option value="<?=$s->id?>" <?php if ($s->id == $order->recipient) : ?>selected="selected"<?php endif; ?>><?=$s->name?></option>
														<?php endforeach; ?>
													<?php endif; ?>
												</select>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-sm btn-primary add-recipient" title="Добавить нового" type="button">+</button>
											</div>
										<?php else : ?>
											<div class="margin-cell">
												<?php if (!empty($stores)) : ?>
													<?php foreach ($stores as $s) : ?>
														<?php if ($s->id == $order->recipient) : ?><?=$s->name?><?php endif; ?>
													<?php endforeach; ?>
												<?php endif; ?>
											</div>
										<?php endif; ?>
									</div>
									<div id="showRecipientsInfo"><!--
										<?php foreach ($stores as $s) : ?>
											<option value="<?=$s->id?>"><?=$s->name?></option>
										<?php endforeach; ?>-->
									</div>
									<div class="hidden" id="hiddenRecipientsData">
										<div class="form-group">
											<label class="col-sm-2 control-label">Наименование</label>
											<div class="col-sm-9">
												<input type="text" name="recipient_name" class="form-control"/>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Адрес</label>
											<div class="col-sm-9 btn-group">
												<textarea name="recipient_address" class="form-control no-resize"></textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Email</label>
											<div class="col-sm-9">
												<input type="text" name="recipient_email_1" def_id="1" class="form-control"/>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-sm btn-primary add-recipient_email" title="Добавить email" type="button">+</button>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-2 control-label">Телефон</label>
											<div class="col-sm-9">
												<input type="text" name="recipient_phone_1" def_id="1" class="form-control" data-mask="+9 (999) 999-99-99"/>
											</div>
											<div class="col-sm-1">
												<button class="btn btn-sm btn-primary add-recipient_phone" title="Добавить телефон" type="button">+</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="row">
								<div class="col-sm-6 b-r">
									<div class="form-group">
										<label class="col-sm-4 control-label">Вес груза</label>
										<div class="col-sm-6">
											<?php if ($can_edit) : ?>
												<input type="text" name="weight" value="<?=$order->weight?>" class="form-control"/>
											<?php else : ?>
												<div class="margin-cell"><?=(!empty($order->weight) ? $order->weight : 0)?></div>
											<?php endif; ?>
										</div>
										<div class="col-sm-2">кг</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label">Кубатура</label>
										<div class="col-sm-6">
											<?php if ($can_edit) : ?>
												<input type="text" name="cubage" value="<?=$order->cubage?>" class="form-control"/>
											<?php else : ?>
												<div class="margin-cell"><?=(!empty($order->cubage) ? $order->cubage : 0)?></div>
											<?php endif; ?>
										</div>
										<div class="col-sm-2">м&#179;</div>
									</div>
									<div class="form-group">
										<div class="col-sm-4">&nbsp;</div>
										<div class="col-sm-8">
											за перевозку вы получите <span id="cubagePoints"><?=(empty($order->cubage) ? 0 : ($order->cubage * Model_Order::$cubage_points))?></span> points
										</div>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="col-sm-4 control-label">Количество мест</label>
										<div class="col-sm-6">
											<?php if ($can_edit) : ?>
												<input type="text" name="capacity" value="<?=$order->capacity?>" class="form-control"/>
											<?php else : ?>
												<div class="margin-cell"><?=(!empty($order->capacity) ? $order->capacity : 0)?></div>
											<?php endif; ?>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-4 control-label">Характер груза</label>
										<div class="col-sm-6">
											<?php if ($can_edit) : ?>
												<input type="text" name="type" value="<?=$order->type?>" class="form-control"/>
											<?php else : ?>
												<div class="margin-cell"><?=(!empty($order->type) ? $order->type : '--')?></div>
											<?php endif; ?>
										</div>
									</div>
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Текущий статус</label>
								<div class="col-sm-4 btn-group">
									<?php if ($can_edit) : ?>
										<select name="state" class="chosen-select">
											<?php if ($is_admin) : ?>
												<?php foreach ($states as $s) : ?>
													<option value="<?=$s->id?>" <?php if ($s->id == $order->state) : ?>selected="selected"<?php endif; ?>><?=$s->title?></option>
												<?php endforeach; ?>
											<?php else : ?>
												<?php foreach ($states as $s) : ?>
													<?php if ($s->id == $order->state) : ?>
														<option value="<?=$s->id?>" selected="selected"><?=$s->title?></option>
													<?php endif; ?>
												<?php endforeach; ?>
											<?php endif; ?>
										</select>
									<?php else : ?>
										<div class="margin-cell">
											<?php foreach ($states as $s) : ?>
												<?php if ($s->id == $order->state) : ?><?=$s->title?><?php endif; ?>
											<?php endforeach; ?>
										</div>
									<?php endif; ?>
								</div>
							</div>
							<?php if (!empty($history)) : ?>
								<div class="form-group"><div><label class="col-sm-2 control-label">История статусов:</label></div></div>
								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-6">
										<table class="table no-margins">
											<?php foreach ($history as $h) : ?>
												<tr>
													<td class="b-r">
														<?php if (date('Y-m-d', strtotime($h['date'])) == date('Y-m-d')) : ?>
															Сегодня,&nbsp;<?=Date::format($h['date'], 'H:i')?>
														<?php elseif (date_diff(date_create(date('Y-m-d')), date_create(Date::format($h['date'], 'Y-m-d')))->format('%a') == 1) : ?>
															Вчера,&nbsp;<?=Date::format($h['date'], 'H:i')?>
														<?php else : ?>
															<?=Date::format($h['date'], 'd F, H:i')?>
														<?php endif; ?>
													</td>
													<td class="b-r"><?=$h['title']?></td>
													<td><?=$h['name']?></td>
												</tr>
											<?php endforeach; ?>
										</table>
									</div>
								</div>
							<?php endif; ?>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-3 btn-group">
									<button class="btn btn-sm btn-primary" id="editRequest">Сохранить</button>
								</div>
							</div>
						<?=Form::close()?>
					<?php else : ?>
						Такой заявки не существует. <a href="/orders/add">Добавить</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>