<div class="row filter">
	<div class="col-lg-7">
                <div class="ibox float-e-margins">
			<div class="ibox-content">
				<div class="row">
					<div class="col-sm-4 b-r">
						<div class="filter-label">Отправитель</div>
						<div class="">
							<select name="sender" class="chosen-select chosen-select-filter">
								<option value="0">Все</option>
								<?php foreach ($stores as $s) : ?>
									<option value="<?=$s->id?>" <?php if (!empty($sender) && $s->id == $sender) : ?>selected="selected"<?php endif; ?>><?=$s->name?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="filter-label">Получатель</div>
						<div class="">
							<select name="recipient" class="chosen-select chosen-select-filter">
								<option value="0">Все</option>
								<?php foreach ($stores as $s) : ?>
									<option value="<?=$s->id?>" <?php if (!empty($recipient) && $s->id == $recipient) : ?>selected="selected"<?php endif; ?>><?=$s->name?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<?php if ($is_admin) : ?>
							<div class="filter-label">Клиент</div>
							<div class="">
								<select name="client_id" class="chosen-select chosen-select-filter">
									<option value="0">Все</option>
									<?php foreach ($clients as $c) : ?>
										<option value="<?=$c->id?>" <?php if (!empty($client_id) && $c->id == $client_id) : ?>selected="selected"<?php endif; ?>><?=$c->name?></option>
									<?php endforeach; ?>
								</select>
							</div>
						<?php else : ?>
							<?=Form::hidden('client_id', $client_id)?>
						<?php endif; ?>
					</div>
					<div class="col-sm-4 b-r">
						<div class="filter-label">Показывать статусы:</div>
						<?php foreach ($states as $s) : ?>
							<?php if ($s['id'] <= 4) : ?>
								<div class="checkbox i-checks">
									<label>
										<div class="icheckbox_square-green">
											<input type="checkbox" value="" class="state-checkbox state-checkbox-filter" check_id="<?=$s['id']?>" checked="checked" style="display:none;" />
											<ins class="iCheck-helper"></ins>
										</div>
										<i></i>
										<?=$s['title'];?>
									</label>
								</div>
							<?php endif; ?>
						<?php endforeach; ?>
					</div>
					<div class="col-sm-4">
						<div class="filter-label filter-cancel text-right"><a href="/orders">Сбросить</a></div>
						<?php foreach ($states as $s) : ?>
							<?php if ($s['id'] > 4) : ?>
								<div class="checkbox i-checks">
									<label>
										<div class="icheckbox_square-green">
											<input type="checkbox" value="" class="state-checkbox state-checkbox-filter" check_id="<?=$s['id']?>" checked="checked" style="display:none;" />
											<ins class="iCheck-helper"></ins>
										</div>
										<i></i>
										<?=$s['title'];?>
									</label>
								</div>
							<?php endif; ?>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
                </div>
	</div>
</div>

<?=View::factory('orders/_list', array('requests' => $requests, 'stores' => $stores, 'pagination' => $pagination, 'total' => $total))?>
