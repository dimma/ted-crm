<div class="wrapper wrapper-content product-info">
	<div class="row animated fadeInRight">
		<div class="col-sm-7">
			<div class="ibox float-e-margins">
				<div class="ibox-content">
					<?php if (!empty($store)) : ?>
						<?=Form::open('/ajax/add_object', array('class' => 'form-horizontal ajax-form', 'id' => 'formEditObject'))?>
							<?=Form::hidden('client_id', $client->id)?>
							<?=Form::hidden('id', $store->id)?>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label class="col-sm-2 control-label">Клиент</label>
										<div class="col-sm-9 request-client"><a href="<?php if ($is_admin) : ?>/clients/edit/<?=$client->id?><?php else : ?>/main/profile<?php endif; ?>"><?=$client->name?></a></div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Наименование</label>
										<div class="col-sm-9">
											<input type="text" name="name" value="<?=$store->name?>" class="form-control"/>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Адрес</label>
										<div class="col-sm-9 btn-group">
											<textarea name="address" class="form-control no-resize"><?=$store->address?></textarea>
										</div>
									</div>
									<?php foreach ($emails as $i => $v) : ?>
										<div class="form-group">
											<?php if ($i == 0) : ?>
												<label class="col-sm-2 control-label">Email</label>
											<?php endif; ?>
											<div class="<?php if ($i != 0) : ?>col-sm-offset-2<?php endif; ?> col-sm-9">
												<input type="text" name="object_email_<?=($i+1)?>" def_id="<?=($i+1)?>" class="form-control" value="<?=$v?>"/>
											</div>
											<?php if ($total_emails == ($i+1)) : ?>
												<div class="col-sm-1">
													<button class="btn btn-sm btn-primary add-object_email" title="Добавить email" type="button">+</button>
												</div>
											<?php endif; ?>
										</div>
									<?php endforeach; ?>
									<?php foreach ($phones as $i => $v) : ?>
										<div class="form-group">
											<?php if ($i == 0) : ?>
												<label class="col-sm-2 control-label">Телефон</label>
											<?php endif; ?>
											<div class="<?php if ($i != 0) : ?>col-sm-offset-2<?php endif; ?> col-sm-9">
												<input type="tel" name="object_phone_<?=($i+1)?>" def_id="<?=($i+1)?>" class="form-control" data-mask="+9 (999) 999-99-99" value="<?=$v?>"/>
											</div>
											<?php if ($total_phones == ($i+1)) : ?>
												<div class="col-sm-1">
													<button class="btn btn-sm btn-primary add-object_phone" title="Добавить телефон" type="button">+</button>
												</div>
											<?php endif; ?>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-3 btn-group">
									<button class="btn btn-sm btn-primary" id="editObject">Сохранить</button>
								</div>
							</div>
						<?=Form::close()?>
					<?php else : ?>
						Такого объекта не существует
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>