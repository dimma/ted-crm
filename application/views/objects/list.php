<div class="wrapper wrapper-content animated fadeInRight">
	<?php if ($stores->count()) : ?>
		<div class="ibox float-e-margins">
			<div class="ibox-content">
				<div class="table-responsive">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th class="thin text-center">&#8470;</th>
								<th>Наименование</th>
								<th>Адрес</th>
								<th>Телефон</th>
								<?php if ($is_admin) : ?><th>Клиент</th><?php endif; ?>
								<th>Дата</th>
							</tr>
						</thead>
						<tbody id="customers-list">
							<?php foreach ($stores as $s) : ?>
								<tr customer_id="<?=$s->id?>">
									<td class="text-center"><?=$s->id?></td>
									<td><a href="/objects/edit/<?=$s->id?>"><?=$s->name?></a></td>
									<td><?=$s->address?></td>
									<td><?=$s->phone?></td>
									<?php if ($is_admin && !empty($clients)) : ?>
										<?php foreach ($clients as $c) : ?>
											<?php if ($c->id == $s->client_id) : ?>
												<td><a href="/clients/edit/<?=$s->client_id?>"><?=$c->name?></a></td>
											<?php endif; ?>
										<?php endforeach; ?>
									<?php endif; ?>
									<td>
										<?php if (date('Y-m-d', strtotime($s->date)) == date('Y-m-d')) : ?>
											Сегодня,&nbsp;<?=Date::format($s->date, 'H:i')?>
										<?php elseif (date_diff(date_create(date('Y-m-d')), date_create(Date::format($s->date, 'Y-m-d')))->format('%a') == 1) : ?>
											Вчера,&nbsp;<?=Date::format($s->date, 'H:i')?>
										<?php else : ?>
											<?=Date::format($s->date, 'd F, H:i')?>
										<?php endif; ?>
									</td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	<?php else : ?>
		<div class="panel colourable">
			<div class="panel-body">
				<p>Список объектов пуст</p>
			</div>
		</div>
	<?php endif; ?>
</div>