<div class="wrapper wrapper-content product-info">
	<div class="row animated fadeInRight">
		<div class="col-sm-7">
			<div class="ibox float-e-margins">
				<div class="ibox-content">
					<?=Form::open('/ajax/add_object', array('class' => 'form-horizontal ajax-form', 'id' => 'formAddObject'))?>
						<?php if (!empty($client_id)) : ?>
							<?=Form::hidden('client_id', $client_id)?>
						<?php endif; ?>
						<div class="row">
							<div class="col-sm-12">
								<?php if (!empty($clients)) : ?>
									<div class="form-group">
										<label class="col-sm-2 control-label">Клиент</label>
										<div class="col-sm-9 btn-group">
											<select name="client_id" class="chosen-select">
												<?php foreach ($clients as $c) : ?>
													<option value="<?=$c->id?>"><?=$c->name?></option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>
								<?php endif; ?>
								<div class="form-group">
									<label class="col-sm-2 control-label">Наименование</label>
									<div class="col-sm-9">
										<input type="text" name="name" class="form-control"/>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Адрес</label>
									<div class="col-sm-9 btn-group">
										<textarea name="address" class="form-control no-resize"></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Email</label>
									<div class="col-sm-9">
										<input type="text" name="object_email_1" def_id="1" class="form-control"/>
									</div>
									<div class="col-sm-1">
										<button class="btn btn-sm btn-primary add-object_email" title="Добавить email" type="button">+</button>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Телефон</label>
									<div class="col-sm-9">
										<input type="text" name="object_phone_1" def_id="1" class="form-control" data-mask="+9 (999) 999-99-99"/>
									</div>
									<div class="col-sm-1">
										<button class="btn btn-sm btn-primary add-object_phone" title="Добавить телефон" type="button">+</button>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-3 btn-group">
								<button class="btn btn-sm btn-primary" id="addObject">Сохранить</button>
							</div>
						</div>
					<?=Form::close()?>
				</div>
			</div>
		</div>
	</div>
</div>