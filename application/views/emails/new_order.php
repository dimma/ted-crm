<p>Поступил заказ с сайта <a href="http://agrodex.ru">agrodex.ru</a></p>
<h3>Покупатель</h3>
<p>Имя: <?=$order->customer->name?></p>
<p>Телефон: <?=$order->customer->phone?></span></p>
<? if($order->customer->email) { ?><p>E-mail <a href="mailto:<?=$order->customer->email?>"><?=$order->customer->email?></a></p><? } ?>
<p>Адрес: <?=$order->customer->address?></p>

<h3>Список заказа</h3>
<table style="width:100%">
	<thead>
		<tr class="CartProduct cartTableHeader">
			<th style="text-align: left;">Наименование</th>
			<th style="width:10%; text-align: right; white-space: nowrap;" >Цена (руб.)</th>
			<th style="width:10%; text-align: right; white-space: nowrap;">Кол-во</th>
			<th style="width:10%; text-align: right; white-space: nowrap;">Сумма (руб.)</th>
		</tr>
	</thead>
	<tbody>
	<? foreach($order->items->find_all() as $item) { ?>
		<tr class="CartProduct">
			<td>
				<h4><a href="http://agrodex.ru/products/<?=$item->product_id?>"><?=$item->product->name?></a></h4>
				<p>
					<span class="size">Длина: <strong><?=$item->length->value?></strong></span><br>
					<span class="size">Комплектация: <strong><?=$item->grade->name?></strong></span><br>
				<? if($item->productaddons->count_all()) { foreach($item->productaddons->find_all() as $addon) { ?>
					<?=$addon->name?> (<?=Num::format($addon->prices->where('option_id','=',$item->length->id)->find()->price,0,'',' ')?>&nbsp;руб.)<br>
				<? } } ?>
				</p>
			</td>
			<td style="text-align: right;"><div class="price"><?=Num::format($item->price,0,'',' ')?></div></td>
			<td style="text-align: right;"><?=$item->count?></td>
			<td style="text-align: right;"><?=Num::format($item->sum,0,'',' ')?></td>
		</tr>
	<? } ?>
	</tbody>
</table>