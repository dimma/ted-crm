<div class="wrapper wrapper-content product-info">
	<div class="row animated fadeInRight">
		<div class="col-sm-7">
			<div class="ibox float-e-margins">
				<div class="ibox-content">
					<?php if (!empty($client->id)) : ?>
						<?=Form::open('/ajax/add_client', array('class' => 'form-horizontal ajax-form', 'id' => 'formEditClient'))?>
							<?=Form::hidden('client_id', $client->id)?>
							<div class="form-group">
								<label class="col-sm-2 control-label">Наименование</label>
								<div class="col-sm-9">
									<input type="text" name="name" class="form-control" value="<?=$client->name;?>"/>
								</div>
							</div>
							<?php foreach ($emails as $i => $v) : ?>
								<div class="form-group">
									<?php if ($i == 0) : ?>
										<label class="col-sm-2 control-label">Email</label>
									<?php endif; ?>
									<div class="<?php if ($i != 0) : ?>col-sm-offset-2<?php endif; ?> col-sm-9">
										<input type="text" name="email_<?=($i+1)?>" def_id="<?=($i+1)?>" class="form-control" value="<?=$v?>"/>
									</div>
									<?php if ($total_emails == ($i+1)) : ?>
										<div class="col-sm-1">
											<button class="btn btn-sm btn-primary add-email" title="Добавить email" type="button">+</button>
										</div>
									<?php endif; ?>
								</div>
							<?php endforeach; ?>
							<?php foreach ($phones as $i => $v) : ?>
								<div class="form-group">
									<?php if ($i == 0) : ?>
										<label class="col-sm-2 control-label">Телефон</label>
									<?php endif; ?>
									<div class="<?php if ($i != 0) : ?>col-sm-offset-2<?php endif; ?> col-sm-9">
										<input type="tel" name="phone_<?=($i+1)?>" def_id="<?=($i+1)?>" class="form-control" data-mask="+9 (999) 999-99-99" value="<?=$v?>"/>
									</div>
									<?php if ($total_phones == ($i+1)) : ?>
										<div class="col-sm-1">
											<button class="btn btn-sm btn-primary add-phone" title="Добавить телефон" type="button">+</button>
										</div>
									<?php endif; ?>
								</div>
							<?php endforeach; ?>
							<div class="form-group">
								<label class="col-sm-2 control-label">Менеджер</label>
								<div class="col-sm-9 btn-group">
									<select data-placeholder="Выбрать менеджера" name="manager_id" class="chosen-select">
										<?php foreach ($managers as $v) : ?>
											<option value="<?=$v->id?>" <?php if ($v->id == $client->manager_id) : ?>selected="selected"<?php endif; ?>>
												<?=$v->name?> <?=$v->lastname?>
											</option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-2 btn-group">
									<button class="btn btn-sm btn-primary" id="editClient">Сохранить</button>
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-2">
									<button class="btn btn-sm btn-primary" id="showRequests" page_id="<?=$client->id;?>" type="button">Посмотреть заявки <br/>(всего: <?=$total?> / активных: <?=$active?>)</button>
								</div>
								<div class="col-sm-offset-1 col-sm-2">
									<button class="btn btn-sm btn-primary" type="button">Отправить SMS<br/> клиенту</button>
								</div>
							</div>
						<?=Form::close()?>
					<?php else : ?>
						Такого клиента не существует
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>