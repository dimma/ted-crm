<div class="wrapper wrapper-content product-info">
	<div class="row animated fadeInRight">
		<div class="col-sm-7">
			<div class="ibox float-e-margins">
				<div class="ibox-content">
					<?=Form::open('/ajax/add_client', array('class' => 'form-horizontal ajax-form', 'id' => 'formAddClient'))?>
						<div class="form-group">
							<label class="col-sm-2 control-label">Наименование</label>
							<div class="col-sm-9">
								<input type="text" name="name" class="form-control"/>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Email</label>
							<div class="col-sm-9">
								<input type="text" name="email_1" def_id="1" class="form-control"/>
							</div>
							<div class="col-sm-1">
								<button class="btn btn-sm btn-primary add-email" title="Добавить email" type="button">+</button>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Телефон</label>
							<div class="col-sm-9">
								<input type="text" name="phone_1" def_id="1" class="form-control" data-mask="+9 (999) 999-99-99"/>
							</div>
							<div class="col-sm-1">
								<button class="btn btn-sm btn-primary add-phone" title="Добавить телефон" type="button">+</button>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Менеджер</label>
							<div class="col-sm-9 btn-group">
								<select data-placeholder="Выбрать менеджера" name="manager_id" class="chosen-select">
									<?php foreach ($managers as $v) : ?>
										<option value="<?=$v->id?>"><?=$v->name?> <?=$v->lastname?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-2 btn-group">
								<button class="btn btn-sm btn-primary" id="addClient">Сохранить</button>
							</div>
						</div>
					<?=Form::close()?>
				</div>
			</div>
		</div>
	</div>
</div>