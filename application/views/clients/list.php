<div class="wrapper wrapper-content animated fadeInRight">
	<?php if (!empty($clients)) : ?>
		<div class="ibox float-e-margins">
			<div class="ibox-content">
				<div class="table-responsive">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th class="thin text-center">&#8470;</th>
								<th>Наименование</th>
								<th class="text-right">Последняя заявка</th>
								<th>Телефон</th>
								<th class="text-right">Points</th>
							</tr>
						</thead>
						<tbody id="customers-list">
							<?php foreach ($clients as $client) : ?>
								<tr customer_id="<?=$client->id?>">
									<td class="text-center"><?=$client->id?></td>
									<td><a href="/clients/edit/<?=$client->id?>"><?=$client->name?></a></td>
									<td class="text-right"><?=(empty($orders[$client->id]) ? '--' : '<a href=/orders/edit/' . $orders[$client->id] . '>' . $orders[$client->id] . '</a>')?></td>
									<td><?=$client->phone?></td>
									<td class="text-right"><?=$client->points?></td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	<?php else : ?>
		<div class="panel colourable">
			<div class="panel-body">
				<p>Список клиентов пуст</p>
			</div>
		</div>
	<?php endif; ?>
</div>