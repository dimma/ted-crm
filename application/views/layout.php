<!DOCTYPE html>
<html>
	<head>
		<title><?=$window_title?></title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="keywords" content="<?=$meta_keywords;?>" />
		<meta name="description" content="<?=$meta_description;?>" />
		<meta name="copyright" content="<?=$meta_copyright;?>" />
		<meta name="author" content="<?=$meta_author;?>" />
		<?php
			foreach ($styles as $file => $type)
			{
				echo HTML::style($file, array('media' => $type)), "\n";
			}
		?>
	</head>
	<body>
		<div id="wrapper">
			<nav class="navbar-default navbar-static-side" role="navigation">
				<div class="sidebar-collapse">
					<ul class="nav" id="side-menu">
						<li class="nav-header">
							<span class="admin-logo profile-element">
								<?=$site_name;?>
								<a href="<?=$tedtrans_url?>" target="_blank" class="pull-right" title="Вернуться на сайт"><i class="fa fa-external-link"></i></a>
							</span>
							<div class="logo-element"><?=$site_name_short;?></div>
						</li>
						<? foreach($menu as $k => $v) { ?>
							<li <? if($controller == $k) { ?>class="active"<? } ?>>
								<a href="/<?=$v['url']?>"><i class="fa fa-<?=$v['icon']?>"></i> <span class="nav-label"><?=$v['name']?></span><? /* <span class="label label-warning pull-right">16/24</span> */ ?><? if(isset($v['items'])) { ?><span class="fa arrow"></span><? } ?></a>
								<? if(isset($v['items'])) { ?>
								<ul class="nav nav-second-level">
									<? foreach($v['items'] as $sub) { ?>
										<li <? if($action == $sub['url']) { ?>class="active" <? } ?>><a href="/<?=$k?>/<?=$sub['url']?>"><?=$sub['name']?></a></li>
									<? } ?>
								</ul>
								<? } ?>
							</li>
						<? } ?>
					</ul>
				</div>
			</nav>
			<div id="page-wrapper" class="gray-bg">
				<div class="row border-bottom">
					<nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
						<div class="navbar-header">
							<a class="navbar-minimalize minimalize-styl-2 btn btn-primary" href="javascript:void(0);" title="Свернуть меню"><i class="fa fa-bars"></i></a>
						</div>
						<?php if (!$is_admin) : ?>
							<div class="navbar-progress">
								<div class="col-sm-4 col-loyal b-r">Silver</div>
								<div class="col-sm-4 col-loyal b-r">Gold</div>
								<div class="col-sm-4 col-loyal">Platinum</div>
								<div class="progress progress-striped active">
									<div class="row">
										<div class="col-sm-4 col-loyal-nav b-r"><?=$points?> points</div>
										<div class="col-sm-4 col-loyal-nav b-r"><?php if (empty($next_loyal)) : ?>&nbsp;<?php endif; ?></div>
										<div class="col-sm-4 col-loyal-nav"></div>
										<div class="progress-text"><?=$next_loyal?></div>
									</div>
									<div style="width: <?=$points_width?>%;" class="progress-bar"></div>
								</div>
							</div>
						<?php endif; ?>
						<ul class="nav navbar-top-links navbar-right">
							<li><span class="m-r-sm text-muted welcome-message"><strong><?=$user_title?></strong></span></li>
							<li class="lnk-logout"><a href="/logout"><i class="fa fa-sign-out"></i> Выйти</a></li>
						</ul>
					</nav>
				</div>
				<div class="row wrapper border-bottom white-bg page-heading">
					<div class="col-sm-7">
						<h1 class="main-title"><?=$title?></h1>
					</div>
					<div class="col-sm-5">
						<div class="title-action">
							<?=$title_content?>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<?=$content?>
					</div>
				</div>
				<div class="footer">
					<div class="pull-right"><a href="http://siteexpo.ru">SiteExpo</a></div>
					<div><strong>&copy;</strong> 2014</div>
				</div>
			</div>
		</div>

		<!-- javascript
		================================================== -->
		<? foreach($scripts as $file) { echo HTML::script($file), "\n"; } ?>
	</body>
</html>
