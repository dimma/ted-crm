<div class="wrapper wrapper-content product-info">
	<div class="row animated fadeInRight">
		<div class="col-sm-7">
			<div class="ibox float-e-margins">
				<div class="ibox-content">
					<?php if (!empty($client->id)) : ?>
						<?=Form::open('/ajax/add_client', array('class' => 'form-horizontal ajax-form', 'id' => 'formEditClientProfile'))?>
							<?=Form::hidden('client_id', $client->id)?>
							<?=Form::hidden('manager_id', $manager->id)?>
							<div class="form-group">
								<label class="col-sm-2 control-label">Наименование</label>
								<div class="col-sm-9">
									<input type="text" name="name" class="form-control" value="<?=$client->name;?>"/>
								</div>
							</div>
							<?php foreach ($emails as $i => $v) : ?>
								<div class="form-group">
									<?php if ($i == 0) : ?>
										<label class="col-sm-2 control-label">Email</label>
									<?php endif; ?>
									<div class="<?php if ($i != 0) : ?>col-sm-offset-2<?php endif; ?> col-sm-9">
										<input type="text" name="email_<?=($i+1)?>" def_id="<?=($i+1)?>" class="form-control" value="<?=$v?>"/>
									</div>
									<?php if ($total_emails == ($i+1)) : ?>
										<div class="col-sm-1">
											<button class="btn btn-sm btn-primary add-email" title="Добавить email" type="button">+</button>
										</div>
									<?php endif; ?>
								</div>
							<?php endforeach; ?>
							<?php foreach ($phones as $i => $v) : ?>
								<div class="form-group">
									<?php if ($i == 0) : ?>
										<label class="col-sm-2 control-label">Телефон</label>
									<?php endif; ?>
									<div class="<?php if ($i != 0) : ?>col-sm-offset-2<?php endif; ?> col-sm-9">
										<input type="tel" name="phone_<?=($i+1)?>" def_id="<?=($i+1)?>" class="form-control" data-mask="+9 (999) 999-99-99" value="<?=$v?>"/>
									</div>
									<?php if ($total_phones == ($i+1)) : ?>
										<div class="col-sm-1">
											<button class="btn btn-sm btn-primary add-phone" title="Добавить телефон" type="button">+</button>
										</div>
									<?php endif; ?>
								</div>
							<?php endforeach; ?>
							<div class="form-group">
								<label class="col-sm-2 control-label">Менеджер</label>
								<div class="col-sm-9 btn-group margin-cell">
									<?=$manager->name?>
									<?php if (!empty($manager->lastname)) : ?>
										<?=$manager->lastname?>
									<?php endif; ?>
									<?php if (!empty($manager->email)) : ?>
										(<a href="mailto:<?=$manager->email?>"><?=$manager->email?></a>)
									<?php endif; ?>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Оповещения</label>
								<div class="col-sm-9 btn-group">
									<div class="checkbox i-checks">
										<label>
											<div class="icheckbox_square-green">
												<input type="checkbox" class="state-checkbox" name="notify_email" <?php if ($client->notify_email) : ?>checked="checked"<?php endif; ?> style="display:none;" />
												<ins class="iCheck-helper"></ins>
											</div>
											<i></i>
											получать письмо-уведомление о смене статусов
										</label>
									</div>
									<div class="checkbox i-checks">
										<label>
											<div class="icheckbox_square-green">
												<input type="checkbox" class="state-checkbox" name="notify_sms" <?php if ($client->notify_sms) : ?>checked="checked"<?php endif; ?> style="display:none;" />
												<ins class="iCheck-helper"></ins>
											</div>
											<i></i>
											получать SMS-уведомление о смене статусов
										</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-2 btn-group">
									<button class="btn btn-sm btn-primary" id="editClientProfile">Сохранить</button>
								</div>
							</div>
						<?=Form::close()?>
					<?php else : ?>
						Такого клиента не существует
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>