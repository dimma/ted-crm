<div class="wrapper wrapper-content animated fadeInRight">
	<div class="col-sm-7">
		<div class="ibox float-e-margins">
			<div class="ibox-content">
				<div class="row">
					<div class="col-sm-4 b-r">
						<div class="form-group text-center">
							<label class="loyalty-label">Silver</label>
							<br/>
							<img src="/assets/img/silver.png" />
						</div>
						<div class="loyalty-list">
							<ul>
								<li>Бесплатный период складского хранения 3 дня</li>
								<li>Отсрочка платежа 5 дней</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-4 b-r">
						<div class="form-group text-center">
							<label class="loyalty-label">Gold</label>
							<br/>
							<img src="/assets/img/gold.png" />
						</div>
						<div class="loyalty-list">
							<ul>
								<li>10% скидка на все перевозки</li>
								<li>Бесплатный период складского хранения 8 дней</li>
								<li>Отсрочка платежа 10 дней</li>
								<li>Бесплатное страхование грузов</li>
								<li>Личный менеджер</li>
							</ul>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group text-center">
							<label class="loyalty-label">Platinum</label>
							<br/>
							<img src="/assets/img/platinum.png" />
						</div>
						<div class="loyalty-list">
							<ul>
								<li>20% скидка на все перевозки</li>
								<li>Бесплатная упаковка груза</li>
								<li>Бесплатный период складского хранения 15 дней</li>
								<li>Отсрочка платежа 14 дней</li>
								<li>Бесплатное страхование грузов</li>
								<li>Личный менеджер</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-sm-7">
		<div class="ibox float-e-margins">
			<div class="ibox-title">Заголовок</div>
			<div class="ibox-content">Описание программы лояльности</div>
		</div>
	</div>
</div>