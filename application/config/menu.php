<?php defined('SYSPATH') or die('No direct access allowed.');
return array(
	'orders' => array(
		'url'	=> 'orders',
		'icon'	=> 'list-alt',
		'name'	=> 'Заявки',
	),
	'profile' => array(
		'url'	=> 'main/profile',
		'icon'	=> 'user',
		'name'	=> 'Профиль',
	),
	'objects' => array(
		'url'	=> 'objects',
		'icon'	=> 'flag',
		'name'	=> 'Объекты',
	),
	'loyalty' => array(
		'url'	=> 'main/loyalty',
		'icon'	=> 'glass',
		'name'	=> 'Программа лояльности',
	),
);
?>