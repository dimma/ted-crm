<?php defined('SYSPATH') or die('No direct access allowed.');
return array(
	'orders' => array(
		'url'	=> 'orders',
		'icon'	=> 'list-alt',
		'name'	=> 'Заявки',
	),
	'clients' => array(
		'url'	=> 'clients',
		'icon'	=> 'users',
		'name'	=> 'Клиенты',
	),
	'objects' => array(
		'url'	=> 'objects',
		'icon'	=> 'flag',
		'name'	=> 'Объекты',
	),
);
?>