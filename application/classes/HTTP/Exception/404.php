<?php defined('SYSPATH') or die('No direct script access.');

class HTTP_Exception_404 extends Kohana_HTTP_Exception_404 {

	/**
	 * Generate a Response for the 404 Exception.
	 *
	 * The user should be shown a nice 404 page.
	 *
	 * @return Response
	 */
	public function get_response()
	{
		# CSS
		$data['styles']['assets/bootstrap/css/bootstrap.css'] = 'screen';			// Bootstrap core CSS
		$data['styles']['assets/css/idangerous.swiper.css'] = 'screen';				// styles needed by swiper slider
		$data['styles']['assets/css/style.css'] = 'screen';							// Custom styles for this template
		$data['styles']['assets/css/layout.css'] = 'screen';						// Project styles
		$data['styles']['assets/css/animate.min.css'] = 'screen';					// css3 animation effect for this template
		$data['styles']['assets/css/owl.carousel.css'] = 'screen';					// styles needed by carousel slider
		$data['styles']['assets/css/owl.theme.css'] = 'screen';						// ...
		$data['styles']['assets/css/ion.checkRadio.css'] = 'screen';				// styles needed by checkRadio
		$data['styles']['assets/css/ion.checkRadio.cloudy.css'] = 'screen';			// ...
		$data['styles']['assets/css/jquery.mCustomScrollbar.css'] = 'screen';		// styles needed by mCustomScrollbar

		# Javascript
		$data['scripts'][] = 'assets/js/jquery/jquery-1.11.1.min.js';
		$data['scripts'][] = 'assets/bootstrap/js/bootstrap.min.js';
		$data['scripts'][] = 'assets/js/idangerous.swiper-2.1.min.js';
		$data['scripts'][] = 'assets/js/jquery.cycle2.min.js';						// jqueryCycle plugin
		$data['scripts'][] = 'assets/js/jquery.easing.1.3.js';						// easing plugin
		$data['scripts'][] = 'assets/js/jquery.parallax-1.1.js';					// parallax plugin
		$data['scripts'][] = 'assets/js/helper-plugins/jquery.mousewheel.min.js';	// optionally include helper plugins
		$data['scripts'][] = 'assets/js/jquery.mCustomScrollbar.js';				// mCustomScrollbar plugin Custom Scrollbar
		$data['scripts'][] = 'assets/js/ion-checkRadio/ion.checkRadio.min.js';		// checkRadio plugin Custom check & Radio
		$data['scripts'][] = 'assets/js/grids.js';									// for equal Div height
		$data['scripts'][] = 'assets/js/owl.carousel.min.js';						// carousel slider plugin
		$data['scripts'][] = 'assets/js/jquery.minimalect.min.js';					// jQuery minimalect // custom select
		$data['scripts'][] = 'assets/js/bootstrap.touchspin.js';					// touch friendly input spinner component
		$data['scripts'][] = 'assets/js/script.js';									// custom script for site
		$data['scripts'][] = 'assets/js/common.js';									// common site scripts

		$view = View::factory('errors/404', $data);

		// Remembering that `$this` is an instance of HTTP_Exception_404
		$view->message = $this->getMessage();
		$response = Response::factory()->status(404)->body($view->render());

		return $response;
	}
}
?>