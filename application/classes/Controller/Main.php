<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Main extends Controller_Layout
{

	public function action_index()
	{
		Controller::redirect('/orders');
	}

	public function action_loyalty()
	{
		$data = array();

		$this->template->title		= 'Программа лояльности';
		$this->template->window_title	= $this->template->title;
		$this->template->content	= View::factory('/main/loyalty', $data);
	}

	public function action_profile()
	{
		$id	= $this->template->user->client_id;
		$client	= ORM::factory('Client', $id);
		$emails	= explode(',', $client->email);
		$phones	= explode(',', $client->phone);
		$data	= array(
			'client'	=> $client,
			'emails'	=> $emails,
			'phones'	=> $phones,
			'total_emails'	=> count($emails),
			'total_phones'	=> count($phones),
			'manager'	=> ORM::factory('User', $client->manager_id),
		);

		$this->template->title		= 'Профиль';
		$this->template->window_title	= $this->template->title;
		$this->template->content	= View::factory('/main/profile', $data);
	}

}
