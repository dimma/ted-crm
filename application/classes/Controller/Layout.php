<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Layout extends Controller_Template
{
	public $template = 'layout';

	public function before()
	{
		parent::before();

		$auth = Auth::instance();

		if ($this->request->controller() !== 'Recovery')
		{
			if ($auth->logged_in())
			{
				$this->user = Auth::instance()->get_user();
			}
			else
			{
				if ($this->request->controller() !== 'Auth' && $this->request->action() !== 'login')
				{
					Controller::redirect('/login');
				}
			}
		}
		else
		{
			$this->user = ($auth->logged_in()) ? Auth::instance()->get_user() : FALSE;
		}

		// Template properties
		if ($this->auto_render)
		{
			$this->template->site_name		= 'TED-CRM';
			$this->template->site_name_short	= 'TED';
			$this->template->tedtrans_url		= 'http://tedtrans.com';
			$this->template->title			= '';
			$this->template->window_title		= '';
			$this->template->meta_keywords		= '';
			$this->template->meta_description	= '';
			$this->template->meta_copyright		= '';
			$this->template->meta_author		= '';
			$this->template->styles			= array();
			$this->template->scripts		= array();
			$this->template->controller		= UTF8::strtolower($this->request->controller());
			$this->template->action			= $this->request->action();
			$this->template->main_page		= ($this->request->controller() == 'main' && $this->request->action() == 'index');
			$this->template->content		= '';
			$this->template->menu			= array();
			$this->template->user_title		= ($auth->logged_in() && isset($this->user)) ? $this->user->name : FALSE;
			$this->template->user			= ($auth->logged_in() && isset($this->user)) ? $this->user : FALSE;
			$this->template->icon			= '';
			$this->template->rights			= array();
			$this->template->title_content		= '';

			// Pagination
			$pagingConfig = Kohana::$config->load('pagination');
			if (!empty($pagingConfig['default']))
			{
				$this->template->paging = $pagingConfig['default'];
			}

			// Rights
			if ($this->template->user)
			{
				$roles = ORM::factory('Role')->find_all();
				foreach ($roles as $role)
				{
					$this->template->rights[$role->name] = FALSE;
				}

				$roles = $this->user->roles->find_all();
				foreach ($roles as $role)
				{
					$this->template->rights[$role->name] = TRUE;
				}
			}

			$isAdmin = !empty($this->template->rights['admin']);
			$this->template->is_admin = $isAdmin;

			// Menu
			$menu = Kohana::$config->load($isAdmin ? 'menu_admin' : 'menu');
			foreach ($menu as $k => $v)
			{
				$this->template->menu[$k] = $v;
			}

			// Current page
			if (isset($menu[$this->template->controller]))
			{
				$this->template->title = $menu[$this->template->controller]['name'];
				$this->template->window_title = $menu[$this->template->controller]['name'] . ' | TED-CRM admin';
			}

			// Get user points
			$this->template->points		= 0;
			$this->template->rest_points	= 0;
			$this->template->points_width	= 1;
			$this->template->next_loyal	= '';
			if (!$isAdmin && !empty($this->template->user->client_id))
			{
				$last = count(Model_Loyalty::$bounds) - 1;
				
				$this->template->points = (int) ORM::factory('Client', $this->template->user->client_id)->get('points');

				if (empty($this->template->points))
				{
					$this->template->rest_points	= Model_Loyalty::$bounds[0]['bound'];
					$this->template->next_loyal	= 'До статуса ' . Model_Loyalty::$bounds[1]['name'] . ' осталось ' . $this->template->rest_points . ' points';
				}
				elseif ($this->template->points > Model_Loyalty::$bounds[$last]['bound'])
				{
					$this->template->points_width = 100;
				}
				else
				{
					foreach (Model_Loyalty::$bounds as $i => $b)
					{
						if ($this->template->points < $b['bound'])
						{
							$this->template->rest_points	= $b['bound'] - $this->template->points;
							$this->template->points_width	= (100 * $this->template->points) / Model_Loyalty::$bounds[$last]['bound'];
							if ($i != $last)
							{
								$this->template->next_loyal = 'До статуса ' . Model_Loyalty::$bounds[$i+1]['name'] . ' осталось ' . $this->template->rest_points . ' points';
							}
							break;
						}
					}
				}
			}
		}
	}

	public function after()
	{
		if ($this->auto_render)
		{
			// Подключаем стандартные стили и скрипты
			$styles['assets/jasny/css/jasny-bootstrap.css']		= 'screen';
			$styles['assets/chosen/chosen.css']			= 'screen';
			$styles['assets/css/plugins/iCheck/custom.css']		= 'screen';
			$styles['assets/css/bootstrap.min.css']			= 'screen';
			$styles['assets/font-awesome/css/font-awesome.css']	= 'screen';
			$styles['assets/css/animate.css']			= 'screen';
			$styles['assets/css/style.css']				= 'screen';
			$styles['assets/css/layout.css']			= 'screen';

			$scripts[] = 'assets/js/jquery-1.11.1.min.js';
			$scripts[] = 'assets/js/bootstrap.min.js';
			$scripts[] = 'assets/js/plugins/metisMenu/jquery.metisMenu.js';
			$scripts[] = 'assets/js/inspinia.js';
			$scripts[] = 'assets/js/plugins/iCheck/icheck.min.js';
			$scripts[] = 'assets/js/plugins/pace/pace.min.js';
			$scripts[] = 'assets/js/plugins/autosize/jquery.autosize.min.js';
			$scripts[] = 'assets/jasny/js/jasny-bootstrap.js';
			$scripts[] = 'assets/chosen/chosen.jquery.js';
			$scripts[] = 'assets/js/common.js';

			// jQuery file upload
			$scripts[] = 'assets/js/plugins/fileupload/jquery.ui.widget.js';
			$scripts[] = 'assets/js/plugins/fileupload/jquery.iframe-transport.js';
			$scripts[] = 'assets/js/plugins/fileupload/jquery.fileupload.js';

			$this->template->styles		= array_merge($this->template->styles, $styles);
			$this->template->scripts	= array_merge($this->template->scripts, $scripts);
			$this->template->window_title .= ' | ' . $this->template->site_name;
		}

		parent::after();
	}
}
?>