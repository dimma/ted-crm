<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Objects extends Controller_Layout {

	public function action_index()
	{
		$stores = ORM::factory('Store');
		if (!empty($this->template->user->client_id))
		{
			$stores = $stores->where('client_id', '=', $this->template->user->client_id);
		}

		$data = array(
			'stores'	=> $stores->find_all(),
			'clients'	=> ORM::factory('Client')->find_all(),
			'is_admin'	=> !empty($this->template->rights['admin']),
		);

		$this->template->title		= 'Объекты';
		$this->template->window_title	= $this->template->title;
		$this->template->title_content	= View::factory('/objects/_top');
		$this->template->content	= View::factory('/objects/list', $data);
	}

	public function action_add()
	{
		$data = array();

		if (empty($this->template->user->client_id))
		{
			$data['clients'] = ORM::factory('Client')->find_all();
		}
		else
		{
			$data['client_id'] = $this->template->user->client_id;
		}

		$this->template->window_title	= 'Новый объект';
		$this->template->title		= '<small><a href="/objects">Объекты</a> / </small> ' . $this->template->window_title;
		$this->template->content	= View::factory('/objects/add', $data);
	}

	public function action_edit()
	{
		$id	= $this->request->param('id');
		$store	= ORM::factory('Store', $id);
		$client	= ORM::factory('Client')->where('id', '=', $store->client_id)->find();
		$emails	= explode(',', $store->email);
		$phones	= explode(',', $store->phone);
		$data	= array(
			'client'	=> $client,
			'store'		=> $store,
			'emails'	=> $emails,
			'phones'	=> $phones,
			'total_emails'	=> count($emails),
			'total_phones'	=> count($phones),
			'is_admin'	=> empty($this->template->user->client_id),
		);

		$this->template->window_title	= $data['store']->name;
		$this->template->title		= '<small><a href="/objects">Объекты</a> / </small> ' . $data['store']->name;
		$this->template->content	= View::factory('/objects/edit', $data);
	}
}
