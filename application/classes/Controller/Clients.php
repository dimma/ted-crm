<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Clients extends Controller_Layout {

	public function action_index()
	{
		$clients	= ORM::factory('Client')->find_all();
		$subSql		= DB::select('id', 'sender', 'recipient', 'client_id')->from('orders')->order_by('date', 'DESC');
		$orders		= DB::select('*')->from(array($subSql, 't'))->group_by('t.client_id')->execute();
		$ordersArr	= $orders->as_array();
		$data		= array('clients' => $clients, 'orders' => array());

		foreach ($ordersArr as $o)
		{
			$data['orders'][$o['client_id']] = $o['id'];
		}

		$this->template->title		= 'Клиенты';
		$this->template->window_title	= $this->template->title;
		$this->template->title_content	= View::factory('/clients/_top');
		$this->template->content	= View::factory('/clients/list', $data);
	}

	public function action_add()
	{
		$managers	= Model_User::getManagers();
		$data		= array('managers' => $managers);

		if (!empty($managers[0]->name))
		{
			$data['default_manager'] = $managers[0]->name;
			if (!empty($managers[0]->lastname))
			{
				$data['default_manager'] .= ' ' . $managers[0]->lastname;
			}
		}

		$this->template->window_title	= 'Новый клиент';
		$this->template->title		= '<small><a href="/clients">Клиенты</a> / </small> ' . $this->template->window_title;
		$this->template->content	= View::factory('/clients/add', $data);
	}

	public function action_edit()
	{
		$id	= $this->request->param('id');
		$client	= ORM::factory('Client', $id);
		$emails	= explode(',', $client->email);
		$phones	= explode(',', $client->phone);
		$data	= array(
			'client'	=> $client,
			'managers'	=> Model_User::getManagers(),
			'total'		=> DB::select('id')->from('orders')->where('client_id', '=', $id)->execute()->count(),
			'active'	=> DB::select('id')->from('orders')->where('client_id', '=', $id)->and_where('state', 'IN', Model_Order::$active_states)->execute()->count(),
			'emails'	=> $emails,
			'phones'	=> $phones,
			'total_emails'	=> count($emails),
			'total_phones'	=> count($phones),
		);

		$this->template->window_title	= $data['client']->name;
		$this->template->title		= '<small><a href="/clients">Клиенты</a> / </small> ' . $data['client']->name;
		$this->template->content	= View::factory('/clients/edit', $data);
	}
}
