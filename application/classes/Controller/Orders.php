<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Orders extends Controller_Layout {

	public function action_index()
	{
		$data		= array();
		$itemsPerPage	= $this->template->paging['items_per_page'];
		$p		= $this->request->query('p');
		$offset		= $itemsPerPage * (empty($p) ? 0 : $p - 1);

		$requests	= Model_Order::getRequests();
		$states		= DB::select('id', 'title')->from('states')->execute();
		$statesArr	= $states->as_array();
		$isAdmin	= (!empty($this->template->rights['admin']));

		$res = array();
		if (!empty($_GET['states']))
		{
			$statesStr = explode('_', $_GET['states']);
			foreach ($statesStr as $k => $v)
			{
				if (!empty($v) && !empty($statesArr[$k]['id']))
				{
					$res[] = $statesArr[$k]['id'];
				}
			}
		}

		if (!empty($res))
		{
			$requests = $requests->where('o.state', 'IN', $res);
		}

		foreach ($_GET as $k => $v)
		{
			switch ($k)
			{
				case 'sender':
				case 'recipient':
				case 'client_id':
					if (!empty($v))
					{
						$requests = $requests->and_where($k, '=', $v);
						$data[$k] = $v;
					}
				break;
			}
		}

		$data['stores'] = ORM::factory('Store');

		if (!$isAdmin)
		{
			$data['client_id']	= $this->template->user->client_id;
			$requests		= $requests->and_where('client_id', '=', $data['client_id']);
			$data['stores']		= $data['stores']->and_where('client_id', '=', $data['client_id']);
		}

		$totalRequests		= $requests->execute()->count();
		$data['states']		= $statesArr;
		$data['stores']		= $data['stores']->find_all()->as_array();
		$data['clients']	= ORM::factory('Client')->find_all()->as_array();
		$data['requests']	= $requests->order_by('o.date', 'DESC')->limit($itemsPerPage)->offset($offset)->execute();
		$data['pagination']	= Pagination::factory(array('items_per_page' => $itemsPerPage, 'total_items' => $totalRequests));
		$data['is_admin']	= $isAdmin;
		$data['total']		= $totalRequests;

		$this->template->window_title	= 'Заявки';
		$this->template->title		= $this->template->window_title;
		$this->template->title_content	= View::factory('orders/_top', $data);
		$this->template->content	= View::factory('orders/index', $data);
	}

	public function action_add()
	{
		$isAdmin	= (!empty($this->template->rights['admin']));
		$stores		= ORM::factory('Store');

		if (!$isAdmin)
		{
			$stores = $stores->where('client_id', '=', $this->template->user->client_id);
		}

		$data = array(
			'stores'	=> $stores->find_all()->as_array(),
			'states'	=> ORM::factory('State')->find_all()->as_array(),
			'cubage_points'	=> Model_Order::$cubage_points,
			'is_admin'	=> $isAdmin,
		);
		if (empty($this->template->user->client_id))
		{
			$data['clients'] =  ORM::factory('Client')->find_all()->as_array();
		}
		else
		{
			$data['client'] = ORM::factory('Client', $this->template->user->client_id);
		}

		$this->template->window_title = 'Новая заявка';
		$this->template->title = '<small><a href="/orders">Заявки</a> / </small> ' . $this->template->window_title;
		$this->template->content = View::factory('orders/add', $data);
	}

	public function action_edit()
	{
		$id	= $this->request->param('id');
		$order	= ORM::factory('Order', $id);
		$hist	= DB::select('os.date', 's.title', 'u.name')
			->from(array('orders_states', 'os'))
			->join(array('states', 's'), 'LEFT')
			->on('s.id', '=', 'os.state_id')
			->join(array('users', 'u'), 'LEFT')
			->on('u.id', '=', 'os.manager_id')
			->where('os.order_id', '=', $id)
			->order_by('os.date', 'DESC')
			->execute();
		$admin	= !empty($this->template->rights['admin']);

		$stores = ORM::factory('Store');

		if (!$admin)
		{
			$stores = $stores->where('client_id', '=', $order->client_id);
		}

		$data = array(
			'order'		=> $order,
			'client'	=> ORM::factory('Client', $order->client_id),
			'stores'	=> $stores->find_all()->as_array(),
			'states'	=> ORM::factory('State')->find_all()->as_array(),
			'history'	=> $hist,
			'cubage_points'	=> Model_Order::$cubage_points,
			'is_admin'	=> $admin,
			'can_edit'	=> ($admin || $order->state == 1),
		);

		$this->template->window_title = 'Заявка #' . $id;
		$this->template->title = '<small><a href="/orders">Заявки</a> / </small> ' . $this->template->window_title;
		$this->template->content = View::factory('orders/edit', $data);
	}

}
