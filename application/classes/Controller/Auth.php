<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Auth extends Controller_Layout
{
	# Authorisation
	# ---------------------------------------------------------------------------------------------- #
	public function action_index()
	{
		Controller::redirect('/login');
	}

	public function action_login()
	{
		$auth = Auth::instance();

		if($this->request->post('username') && $this->request->post('password'))
		{
			$remember = TRUE;
			$error = array();

			try
			{
				$post = Validation::factory($this->request->post())
					->rule('username', 'trim')
					->rule('username', 'not_empty')
					->rule('password', 'trim')
					->rule('password', 'not_empty');

				if ($post->check() && $auth->login($post['username'], $post['password'], $remember))
				{
					Controller::redirect('/');
				}
				else
				{
					$error['login_falied'] = TRUE;
				}
			}
			catch (ORM_Validation_Exception $e)
			{
				//Kohana::ar($e->errors());
			}
			catch (Exception $e)
			{
				// Do your error handling
			}
		}

		if (!$auth->logged_in())
		{
			$this->template->title = 'Авторизация';
			$this->template->set_filename('/auth/login')->bind('error', $error);
		}
		else
		{
			Controller::redirect('/');
		}
	}

	public function action_register()
	{
		$auth = Auth::instance();
		$post = $this->request->post();

		if (!empty($post['username']) && !empty($post['password']))
		{
			$remember	= TRUE;
			$error		= array();
			$date		= Date::formatted_time();

			$regData = array(
				'username'		=> $post['username'],
				'password'		=> $post['password'],
				'password_confirm'	=> $post['password'],
				'name'			=> $post['name'],
				'email'			=> $post['email'],
				'title'			=> 'Клиент',
				'client_id'		=> NULL,
				'date'			=> $date,
			);
			$clientData = array(
				'name'		=> $post['name'],
				'email'		=> $post['email'],
				'phone'		=> $post['phone'],
				'manager_id'	=> 1,
				'date'		=> $date,
			);

			try
			{
				$regData['client_id'] = ORM::factory('Client')->values($clientData)->save();

				$user = ORM::factory('User')->create_user($regData, array('username', 'password', 'name', 'email', 'title', 'client_id', 'date'));
				$user->add('roles', ORM::factory('role', array('name' => 'login')));
				$user->add('roles', ORM::factory('role', array('name' => 'client')));

				if ($auth->login($post['username'], $post['password'], $remember))
				{
					Controller::redirect('/');
				}
			}
			catch (ORM_Validation_Exception $e)
			{
				// Set failure message
				$message = 'There were errors, please see form below.';
				// Set errors using custom messages
				$errors = $e->errors('models');
				Kohana::ar($errors);
			}
		}

		if (!$auth->logged_in())
		{
			$this->template->title = 'Регистрация';
			$this->template->set_filename('/auth/register')->bind('error', $error);
		}
		else
		{
			Controller::redirect('/');
		}
	}

	# Logout
	# ---------------------------------------------------------------------------------------------- #
	public function action_logout()
	{
		if(Auth::instance()->logged_in())
		{
			Auth::instance()->logout();
		}

		Controller::redirect('/');
	}

	# Add user
	public function action_adduser()
	{
		$pass = rand(11111111,99999999);	// 40390236

		$reg_data = array(
			'name'			=> 'Администратор',
			'email'			=> 'rustam@otono.ru',
			'username'		=> 'admin',
			'password'		=> $pass,
			'password_confirm'	=> $pass,
			'date'			=> Date::formatted_time()
		);

		try
		{
			$user = ORM::factory('user')->create_user($reg_data, array('name', 'lastname', 'title', 'email', 'username', 'password', 'date'));
			$user->add('roles', ORM::factory('role', array('name' => 'login')));
			$user->add('roles', ORM::factory('role', array('name' => 'admin')));

			echo 'user created - '.$pass;
		}
		catch(ORM_Validation_Exception $e)
		{
			// Set failure message
			$message = 'There were errors, please see form below.';
			// Set errors using custom messages
			$errors = $e->errors('models');
			Kohana::ar($errors);
		}

		die();
	}

}
?>