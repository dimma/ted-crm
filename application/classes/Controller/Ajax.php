<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Ajax extends Controller
{

	public function action_add_client()
	{
		$post = $this->request->post();
		if (!empty($post['form_data']))
		{
			try
			{
				$json		= array('status' => 0);
				$insertData	= array(
					'name'		=> '',
					'email'		=> '',
					'phone'		=> '',
					'manager_id'	=> 1,
					'notify_email'	=> 1,
					'notify_sms'	=> 1,
					'date'		=> Date::formatted_time(),
				);
				foreach ($post['form_data'] as $v)
				{
					$email = strstr($v['name'], 'email_');
					if (!empty($email))
					{
						if (Valid::not_empty($v['value']))
						{
							if (Valid::email($v['value']))
							{
								if ($email != 'email_1')
								{
									$insertData['email'] .= ',';
								}
								$insertData['email'] .= $v['value'];
							}
							else
							{
								$json['errors_add'][$email][] = 'not_valid';
							}
						}
						else
						{
							$json['errors_add'][$email][] = 'not_empty';
						}
					}

					$phone = strstr($v['name'], 'phone_');
					if (!empty($phone))
					{
						if (Valid::not_empty($v['value']))
						{
							if (Valid::phone($v['value']))
							{
								if ($phone != 'phone_1')
								{
									$insertData['phone'] .= ',';
								}
								$insertData['phone'] .= $v['value'];
							}
							else
							{
								$json['errors_add'][$phone][] = 'not_valid';
							}
						}
						else
						{
							$json['errors_add'][$phone][] = 'not_empty';
						}
					}
				}

				if (!empty($post['manager_id']))
				{
					$insertData['manager_id'] = $post['manager_id'];
				}

				if (!empty($post['name']))
				{
					$insertData['name'] = htmlspecialchars(trim($post['name']));
				}
				else
				{
					$json['errors']['name'] = 'not_empty';
				}

				if (!empty($post['notify_email']))
				{
					$insertData['notify_email'] = (int) ($post['notify_email'] === 'true');
				}
				if (!empty($post['notify_sms']))
				{
					$insertData['notify_sms'] = (int) ($post['notify_sms'] === 'true');
				}

				if (empty($post['client_id']))
				{
					ORM::factory('Client')->values($insertData)->save();
				}
				else
				{
					ORM::factory('Client', $post['client_id'])->values($insertData)->update();
				}

				$json['status'] = 1;
			}
			catch (ORM_Validation_Exception $e)
			{
				$json['errors'] = $e->errors();
				$json['status'] = 0;
			}

			echo json_encode($json);
		}

		exit;
	}

	public function action_add_request()
	{
		$post = $this->request->post();
		if (!empty($post))
		{
			try
			{
				$json = array();

				if (empty($post['client_id']))
				{
					$json['errors']['client_id'] = 'Клиент не может быть пустым';
				}
				// Sender from input field
				if (!empty($post['add_sender']) && $post['add_sender'] === 'true')
				{
					foreach ($post['form_data'] as $f)
					{
						if ($f['name'] === 'sender_name' && empty($f['value']))
						{
							$json['errors']['sender_name'] = 'Отправитель не может быть пустым';
						}
					}
				}
				else
				{
					if (empty($post['sender']))
					{
						$json['errors']['sender'] = 'Отправитель не может быть пустым';
					}
				}
				// Recipient from input field
				if (!empty($post['add_recipient']) && $post['add_recipient'] === 'true')
				{
					foreach ($post['form_data'] as $f)
					{
						if ($f['name'] === 'recipient_name' && empty($f['value']))
						{
							$json['errors']['recipient_name'] = 'Получатель не может быть пустым';
						}
					}
				}
				else
				{
					if (empty($post['recipient']))
					{
						$json['errors']['recipient'] = 'Получатель не может быть пустым';
					}
				}

				$date	= Date::formatted_time();
				$state	= (empty($post['state']) ? 1 : $post['state']);

				$insertData = array(
					'client_id'	=> $post['client_id'],
					'state'		=> $state,
					'sender'	=> $post['sender'],
					'recipient'	=> $post['recipient'],
					'weight'	=> (empty($post['weight']) ? '' : $post['weight']),
					'cubage'	=> (empty($post['cubage']) ? '' : $post['cubage']),
					'capacity'	=> (empty($post['capacity']) ? '' : $post['capacity']),
					'type'		=> (empty($post['type']) ? '' : $post['type']),
					'date'		=> $date,
				);

				$auth = Auth::instance();
				$ordersStateInsertData = array(
					'state_id'	=> $state,
					'manager_id'	=> $auth->get_user()->id,
					'date'		=> $date,
				);
				$sendInsertData = array(
					'name'		=> '',
					'address'	=> '',
					'email'		=> '',
					'phone'		=> '',
					'client_id'	=> 0,
					'date'		=> $date,
				);
				$recipInsertData = $sendInsertData;

				foreach ($post['form_data'] as $v)
				{
					if ($v['name'] === 'sender_name')
					{
						$sendInsertData['name'] = htmlspecialchars(trim($v['value']));
					}
					if ($v['name'] === 'sender_address')
					{
						$sendInsertData['address'] = $v['value'];
					}

					$email = strstr($v['name'], 'sender_email_');
					if (!empty($email))
					{
						if (Valid::not_empty($v['value']))
						{
							if (Valid::email($v['value']))
							{
								if ($email != 'sender_email_1')
								{
									$sendInsertData['email'] .= ',';
								}
								$sendInsertData['email'] .= $v['value'];
							}
							else
							{
								$json['errors'][$email] = 'not_valid';
							}
						}
					}

					$phone = strstr($v['name'], 'sender_phone_');
					if (!empty($phone))
					{
						if (Valid::not_empty($v['value']))
						{
							if (Valid::phone($v['value']))
							{
								if ($phone != 'sender_phone_1')
								{
									$sendInsertData['phone'] .= ',';
								}
								$sendInsertData['phone'] .= $v['value'];
							}
							else
							{
								$json['errors'][$phone] = 'not_valid';
							}
						}
					}
				}

				if (!empty($sendInsertData['name']))
				{
					$sendInsertData['client_id']	= $post['client_id'];
					$insertData['sender']		= ORM::factory('Store')->values($sendInsertData)->save();
				}

				foreach ($post['form_data'] as $v)
				{
					if ($v['name'] === 'recipient_name')
					{
						$recipInsertData['name'] = htmlspecialchars(trim($v['value']));
					}
					if ($v['name'] === 'recipient_address')
					{
						$recipInsertData['address'] = $v['value'];
					}

					$email = strstr($v['name'], 'recipient_email_');
					if (!empty($email))
					{
						if (Valid::not_empty($v['value']))
						{
							if (Valid::email($v['value']))
							{
								if ($email != 'recipient_email_1')
								{
									$recipInsertData['email'] .= ',';
								}
								$recipInsertData['email'] .= $v['value'];
							}
							else
							{
								$json['errors'][$email] = 'not_valid';
							}
						}
					}

					$phone = strstr($v['name'], 'recipient_phone_');
					if (!empty($phone))
					{
						if (Valid::not_empty($v['value']))
						{
							if (Valid::phone($v['value']))
							{
								if ($phone != 'recipient_phone_1')
								{
									$recipInsertData['phone'] .= ',';
								}
								$recipInsertData['phone'] .= $v['value'];
							}
							else
							{
								$json['errors'][$phone] = 'not_valid';
							}
						}
					}
				}

				if (!empty($recipInsertData['name']))
				{
					$recipInsertData['client_id']	= $post['client_id'];
					$insertData['recipient']	= ORM::factory('Store')->values($recipInsertData)->save();
				}

				if (empty($json['errors']))
				{
					if (!empty($post['order_id']))
					{
						$ordersStateInsertData['order_id'] = ORM::factory('Order', $post['order_id'])->values($insertData)->update();
					}
					else
					{
						$ordersStateInsertData['order_id'] = ORM::factory('Order')->values($insertData)->save();
					}

					$prevState = ORM::factory('Orders_State')
						->where('order_id', '=', $ordersStateInsertData['order_id'])
						->order_by('date', 'DESC')
						->find()
						->as_array();
					if (empty($prevState['state_id']) || $state != $prevState['state_id'])
					{
						ORM::factory('Orders_State')->values($ordersStateInsertData)->save();
					}

					// Calculate all client points
					if ($state == Model_Order::$last_state)
					{
						$points = DB::select(array(DB::expr('SUM(cubage * 100)'), 'total_points'))
							->from('orders')
							->where('client_id', '=', 1)
							->and_where('state', '=', Model_Order::$last_state)
							->limit(1)
							->execute();
						$points = $points->as_array();

						if (!empty($points[0]['total_points']))
						{
							ORM::factory('Client', $post['client_id'])->values(array('points' => $points[0]['total_points']))->update();
						}
					}

					$json['status'] = 1;
				}
			}
			catch (ORM_Validation_Exception $e)
			{
				$json['errors'] = $e->errors();
				$json['status'] = 0;
			}

			echo json_encode($json);
		}

		exit;
	}

	public function action_add_object()
	{
		$post = $this->request->post();
		if (!empty($post))
		{
			try
			{
				$json = array();

				if (empty($post['client_id']))
				{
					$json['errors']['client_id'] = 'Клиент не может быть пустым';
				}
				if (empty($post['name']))
				{
					$json['errors']['name'] = 'Объект не может быть пустым';
				}

				$insertData = array(
					'client_id'	=> $post['client_id'],
					'name'		=> $post['name'],
					'address'	=> (empty($post['address']) ? '' : $post['address']),
					'phone'		=> '',
					'email'		=> '',
					'date'		=> Date::formatted_time(),
				);

				foreach ($post['form_data'] as $v)
				{
					$email = strstr($v['name'], 'object_email_');
					if (!empty($email))
					{
						if (Valid::not_empty($v['value']))
						{
							if (Valid::email($v['value']))
							{
								if ($email != 'object_email_1')
								{
									$insertData['email'] .= ',';
								}
								$insertData['email'] .= $v['value'];
							}
							else
							{
								$json['errors'][$email] = 'not_valid';
							}
						}
					}

					$phone = strstr($v['name'], 'object_phone_');
					if (!empty($phone))
					{
						if (Valid::not_empty($v['value']))
						{
							if (Valid::phone($v['value']))
							{
								if ($phone != 'object_phone_1')
								{
									$insertData['phone'] .= ',';
								}
								$insertData['phone'] .= $v['value'];
							}
							else
							{
								$json['errors'][$phone] = 'not_valid';
							}
						}
					}
				}

				if (empty($json['errors']))
				{
					if (empty($post['id']))
					{
						ORM::factory('Store')->values($insertData)->save();
					}
					else
					{
						ORM::factory('Store', $post['id'])->values($insertData)->update();
					}
					$json['status'] = 1;
				}
			}
			catch (ORM_Validation_Exception $e)
			{
				$json['errors'] = $e->errors();
				$json['status'] = 0;
			}

			echo json_encode($json);
		}

		exit;
	}

	public function action_filter_orders()
	{
		$post = $this->request->post();
		if (!empty($post))
		{
			$json = array();

			if (!empty($post['states']) && !empty($post['select']))
			{
				$states		= DB::select('id')->from('states')->execute();
				$statesArr	= $states->as_array();
				$res		= array();
				$data		= array();

				foreach ($post['states'] as $k => $v)
				{
					if ($v === 'true' && !empty($statesArr[$k]['id']))
					{
						$res[] = $statesArr[$k]['id'];
					}
				}

				$req = Model_Order::getRequests();

				if (!empty($res))
				{
					$req = $req->where('o.state', 'IN', $res);
				}

				foreach ($post['select'] as $k => $v)
				{
					switch ($k)
					{
						case 'sender':
						case 'recipient':
						case 'client_id':
							if (!empty($v))
							{
								$req = $req->and_where($k, '=', $v);
							}
						break;
					}
				}

				$itemsPerPage		= 50;
				$p			= $this->request->query('p');
				$offset			= $itemsPerPage * (empty($p) ? 0 : $p - 1);
				$requests		= $req->order_by('o.date', 'DESC');
				$totalRequests		= $requests->execute()->count();
				$data['requests']	= $requests->limit($itemsPerPage)->offset($offset)->execute();
				$data['pagination']	= Pagination::factory(array('items_per_page' => $itemsPerPage, 'total_items' => $totalRequests));
				$data['stores']		= ORM::factory('Store')->find_all()->as_array();
				$data['clients']	= ORM::factory('Client')->find_all()->as_array();
				$data['is_admin']	= (!empty($this->template->rights['admin']));
				$data['total']		= $totalRequests;

				$json['status']	= 1;
				$json['html']	= View::factory('orders/_list', $data)->__toString();
			}

			echo json_encode($json);
		}

		exit;
	}

	/**
	 * Send SMS
	 * usage: $this->_sendSms($number, array('order_id' => $order_id, 'state_name' => $state_name));
	 * @access protected
	 * @param string $number
	 * @param array $data
	 */
	protected function _sendSms($number, $data)
	{
		$xml = '<?xml version="1.0" encoding="UTF-8"?>'
			+ '<SMS>'
			+ '<operations><operation>SEND</operation></operations>'
			+ '<authentification>'
				+ '<username>info@tedtrans.com</username>'
				+ '<password>10Euro</password>'
			+ '</authentification>'
			+ '<message>'
				+ '<sender>Tedtrans</sender>'
				+ '<text>Ваша заявка №' . $data['order_id'] . ' изменила статус на ' . $data['state_name'] . '</text>'
			+ '</message>'
			+ '<numbers><number>' . $number . '</number></numbers>'
			+ '</SMS>';

		$curl = curl_init();
		$curlOptions = array(
			CURLOPT_URL		=> 'http://atompark.com/members/sms/xml.php',
			CURLOPT_FOLLOWLOCATION	=> FALSE,
			CURLOPT_POST		=> TRUE,
			CURLOPT_HEADER		=> FALSE,
			CURLOPT_RETURNTRANSFER	=> TRUE,
			CURLOPT_CONNECTTIMEOUT	=> 15,
			CURLOPT_TIMEOUT		=> 100,
			CURLOPT_POSTFIELDS	=> array('XML' => $xml),
		);
		curl_setopt_array($curl, $curlOptions);
		curl_exec($curl);
		curl_close($curl);
	}
}
