<?php defined('SYSPATH') or die('No direct script access.');

class Model_Order extends ORM
{
	static public $active_states	= array(1, 2, 3, 4, 5, 6);
	static public $cubage_points	= 100;
	static public $last_state	= 8;

	static public function getRequests()
	{
		return DB::select('o.*', array('s.id', 'state_id'), array('s.name', 'state_name'), array('s.title', 'state_title'), array('c.name', 'client_name'))
			->from(array('orders', 'o'))
			->join(array('states', 's'), 'LEFT')
			->on('o.state', '=', 's.id')
			->join(array('clients', 'c'), 'LEFT')
			->on('o.client_id', '=', 'c.id');
	}
}
?>