<?php defined('SYSPATH') or die('No direct script access.');

class Model_Loyalty extends ORM
{

	static public $bounds = array(
		0 => array(
			'name'	=> 'Silver',
			'bound'	=> 1000,
		),
		1 => array(
			'name'	=> 'Gold',
			'bound'	=> 2000,
		),
		2 => array(
			'name'	=> 'Platinum',
			'bound'	=> 3000,
		),
	);

}
?>