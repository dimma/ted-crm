<?php defined('SYSPATH') or die('No direct script access.');

class Model_Client extends ORM
{

	public function filters()
	{
		return array(
			TRUE => array(
				array('trim'),
				array('stripslashes'),
			),
		);
	}

	public function rules()
	{
		return array(
			'name'	=> array(array('not_empty')),
			'email'	=> array(array('not_empty')),
			'phone'	=> array(array('not_empty')),
		);
	}
	
}
?>